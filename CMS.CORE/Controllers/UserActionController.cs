﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CMS.CORE.Model;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.CORE.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class UserActionController : ControllerBase
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;
        public UserActionController(
              UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }
        // GET: api/<UserActionController>
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginVM model)
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user != null && !String.IsNullOrEmpty(user.Id.ToString()))
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, true);

                if (!result.Succeeded)
                    return BadRequest("Mật khẩu không đúng");
                
                var claims = new[]
                {
                    new Claim("Email", user.Email),
                    new Claim(SystemConstants.UserClaim.Id, user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(SystemConstants.UserClaim.FullName, user.FullName ?? "Guest"),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(_configuration["Tokens:Issuer"],
                    _configuration["Tokens:Issuer"],
                    claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: creds);

                var tokenJwt = new JwtSecurityTokenHandler().WriteToken(token);

                var refreshToken = new JwtSecurityToken(_configuration["Tokens:Issuer"],
                   _configuration["Tokens:Issuer"],
                   claims,
                   expires: DateTime.Now.AddDays(1),
                   signingCredentials: creds);

                var refreshtokenJwt = new JwtSecurityTokenHandler().WriteToken(refreshToken);

                var currentUser = new CurrentUserVM()
                {
                    Email = user.Email,
                    Id = user.Id.ToString(),
                    FullName = user.FullName,
                    UserName = user.UserName,
                };
                response.AccessToken = token;
                response.RefreshToken = refreshtokenJwt;
                response.UserData = currentUser;
                return Ok(response);
            }


            return NotFound(response);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("register")]
        public async Task<IActionResult> Register()
        {
            var user = new AppUser
            { FullName = "Triệu Văn Thắng", UserName = "admin", Email = "admin@gmail.com" };
            var result = await _userManager.CreateAsync(user, "Admin123@gmail.com");

            if (result.Succeeded)
            {
                return Ok(user);
            }
            return BadRequest();
        }

        // GET api/<UserActionController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<UserActionController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<UserActionController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UserActionController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
