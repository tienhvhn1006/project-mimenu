﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.CORE.Model
{
    public class LoginVM
    {
        [Required] public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
    public class CurrentUserVM
    {
        public string Email { get; set; }
        public string Id { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string PermissionUrl { get; set; }
        public string Permissions { get; set; }
        public string Address { get; set; }

        public List<Ability> Ability { get; set; }
        public Extras Extras { get; set; }

        public CurrentUserVM()
        {
            this.Avatar = "/img/13-small.d796bffd.png";
            this.Role = "admin";
            this.Ability = new List<Ability>();
            this.Extras = new Extras();
        }
    }



    public class Ability
    {
        public string Action { get; set; }
        public string Subject { get; set; }
        public Ability()
        {
            this.Action = "manage";
            this.Subject = "all";
        }
    }
    public class Extras
    {
        public int ECommerceCartItemsCount { get; set; }

        public Extras()
        {
            this.ECommerceCartItemsCount = 5;

        }
    }


    
}
