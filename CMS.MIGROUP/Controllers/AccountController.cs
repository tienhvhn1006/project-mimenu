﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Setup;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using CORE.UTILITY;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace CMS.MIGROUP.Controllers
{
    public class AccountController : Controller
    {

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        private readonly IAppUserService _appUserService;
        private readonly IEnterprisesService _enterprisesService;

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager
            , ILogger<AccountController> logger, IAppUserService appUserService, IEnterprisesService enterprisesService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appUserService = appUserService;
            _enterprisesService = enterprisesService;

        }
        // GET: AccountController
        [HttpGet("dang-nhap")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Authen(LoginVM model)
        {
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user != null && user.Status == Status.Active)
            {
                var enterprise = _enterprisesService.FindById(user.AppId);
                if (enterprise != null && !String.IsNullOrEmpty(enterprise.Id))
                {
                    var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        if (!String.IsNullOrEmpty(user.AppId))
                        {
                            var Roles = await _userManager.GetRolesAsync(user);

                            var IsGet = await _userManager.GetClaimsAsync(user);
                            var isDelete = await _userManager.RemoveClaimsAsync(user, IsGet);

                            string logo = enterprise.Avatar;

                            List<Claim> Clams = new List<Claim>()
                        {
                            new Claim(SystemConstants.UserClaim.FullName, user.FullName),
                            new Claim(SystemConstants.UserClaim.AppId, user.AppId ?? ""),
                            new Claim(SystemConstants.UserClaim.Logo, logo),
                            new Claim(ClaimTypes.Role, String.Join(",",Roles)),
                             new Claim(SystemConstants.UserClaim.Domain,enterprise.Domain),
                        };
                            var isAdd = await _userManager.AddClaimsAsync(user, Clams);
                            await _signInManager.RefreshSignInAsync(user);
                            return new OkObjectResult(new GenericResult(true));
                        }
                        else
                        {
                            return new ObjectResult(new GenericResult(false, "Tài khoản chưa kích hoạt"));
                        }

                    }
                    if (result.IsLockedOut)
                    {
                        return new ObjectResult(new GenericResult(false, "Tài khoản đã bị khoá"));
                    }
                    else
                    {
                        return new ObjectResult(new GenericResult(false, "Đăng nhập sai"));
                    }
                }
                else
                {
                    return new ObjectResult(new GenericResult(false, "Tài khoản bạn chưa được kích hoạt"));
                }

            }
            return new ObjectResult(new GenericResult(false, "Thông tin không phù hợp"));
        }
        [HttpGet("quen-mat-khau")]
        public ActionResult ForgotPass()
        {
            return View();
        }
        [HttpPost("submit-quen-mat-khau")]
        public async Task<ActionResult> ForgotPass(string Email)
        {
            var user = await _userManager.FindByEmailAsync(Email);
            if (user != null)
            {
                await _appUserService.GenerateForgotPasswordTokenAsync(user);
                return new ObjectResult(new GenericResult(true, "Hệ thống đã gửi Email. Bạn kiểm tra Email và làm theo hướng dẫn."));
            }

            return new ObjectResult(new GenericResult(false, "Không tìm thấy tài khoản."));
        }
        [HttpGet("reset-password")]
        public IActionResult ResetPassword(string uid, string token)
        {
            if (!String.IsNullOrEmpty(uid) && !String.IsNullOrEmpty(token))
            {
                ResetPasswordVM resetPasswordModel = new ResetPasswordVM
                {
                    Token = token,
                    UserId = uid
                };
                return View(resetPasswordModel);
            }
            else
            {
                return Redirect("/");
            }
        }
        //[HttpPost("reset-password")]
        //public async Task<ActionResult> AResetPassword(ResetPasswordVM model)
        //{
        //    model.Token = model.Token.Replace(' ', '+');
        //    var result = await _appUserService.ResetPasswordAsync(model);
        //    if (result.Succeeded)
        //    {
        //        return new ObjectResult(new GenericResult(true, "Reset mật khẩu thành công"));
        //    }

        //    return new ObjectResult(new GenericResult(false, "Reset mật khẩu thất bại"));
        //}

        [HttpPost("m-reset-password")]
        public async Task<ActionResult> ResetPassword(ResetPasswordVM model)
        {
            if(model.NewPassword == model.ConfirmNewPassword)
            {
                model.Token = model.Token.Replace(' ', '+');
                var result = await _appUserService.ResetPasswordAsync(model);
                if (result.Succeeded)
                {
                    return new ObjectResult(new GenericResult(true, "Reset mật khẩu thành công"));
                }

                return new ObjectResult(new GenericResult(false, "Reset mật khẩu thất bại"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Mật khẩu không trùng nhau"));
            }
        }
        //[AllowAnonymous, HttpPost("reset-password")]
        //public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        model.Token = model.Token.Replace(' ', '+');
        //        var result = await _accountRepository.ResetPasswordAsync(model);
        //        if (result.Succeeded)
        //        {
        //            ModelState.Clear();
        //            model.IsSuccess = true;
        //            return View(model);
        //        }

        //        foreach (var error in result.Errors)
        //        {
        //            ModelState.AddModelError("", error.Description);
        //        }
        //    }
        //    return View(model);
        //}

        public async Task<IActionResult> Logout()
        {

            await _signInManager.SignOutAsync();
            return Redirect("/");
        }



    }

}
