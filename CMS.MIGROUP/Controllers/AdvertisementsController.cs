﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class AdvertisementsController : BaseController
    {
        IAdvertisementsService _advertisementsService;
        public AdvertisementsController(IAdvertisementsService advertisementsService)
        {
            _advertisementsService = advertisementsService;
        }

        public IActionResult Index()
        {
            var lstData = _advertisementsService.FindAll().ToList();
            return View(lstData);
        }
        public IActionResult Info(int id = 0)
        {
            Advertisement current = new Advertisement();
            if (id > 0)
            {
                current = _advertisementsService.FindById(id);
                if (current == null)
                {
                    current = new Advertisement() { AppId = currentAppId };
                }
            }
            return View(current);

        }
        [HttpPost]
        public IActionResult Add(Advertisement obj)
        {
            if (obj.Id > 0)
            {
                obj.DateModified = DateTime.Now;
            }
            else
            {
                obj.AppId = currentAppId;
                obj.DateCreated = DateTime.Now;
                obj.DateModified = DateTime.Now;
            }
            var iSuccess = _advertisementsService.AddAds(obj);
            if (iSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
        }
        public IActionResult Delete(int id)
        {
            var isSuccess = _advertisementsService.Remove(new Advertisement() { Id = id }, true);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
    }
}
