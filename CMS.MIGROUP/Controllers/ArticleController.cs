﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.Setup;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class ArticleController : BaseController
    {

        public IProductService _productService;

        public ArticleController(IProductService productService)
        {
            _productService = productService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData([FromQuery] Filter filter)
        {

            int recordsTotal = 0;
            var result = _productService.LoadDataNews(filter, TypeNews.Blog, TypeContent.News, out recordsTotal);

            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = result });
        }

        public IActionResult Delete(int id)
        {
            var isSuccess = _productService.DeleteArticle(id);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
        public IActionResult Info(int id = 0)
        {
            Product current = new Product() { Type = TypeContent.News, TypePost = TypeNews.Blog, Code = MUtility.GenCode("AR"), AppId = currentAppId };
            if (id > 0)
            {
                current = _productService.FindById(id);
                if (current == null)
                {
                    return Redirect("/");
                }
            }
            return View(current);
        }
        [HttpPost]
        public IActionResult Add(Product product)
        {

            if (product.Id <= 0)
            {
                product.AppId = currentAppId;
                product.DateCreated = DateTime.Now;
                product.DateModified = DateTime.Now;
                product.DatePublished = DateTime.Now;
                product.CreatedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                product.ModifiedBy = product.CreatedBy;
            }
            else
            {
                product.DateModified = DateTime.Now;
                product.ModifiedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
            var isSuccess = _productService.AddProduct(product);
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }

        }
    }
}
