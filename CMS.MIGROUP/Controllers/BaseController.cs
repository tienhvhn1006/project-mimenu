﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static CORE.UTILITY.SystemConstants;

namespace CMS.MIGROUP.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {

        public ClaimsPrincipal currentUser
        {
            get { return this.User; }
        }

        public string currentAppId
        {
            get
            {
                try
                {
                    return this.User.FindFirst(UserClaim.AppId).Value;
                }
                catch (Exception)
                {


                }
                return "";

            }
        }
    }
}
