﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;


namespace CMS.MIGROUP.Controllers
{
    public class ColorController : BaseController
    {
        IColorService _colorService;
        public ColorController(IColorService colorService)
        {
            _colorService = colorService;
        }
        public IActionResult Index()
        {
            var color = this.currentUser;
            var data = _colorService.FindAll().ToList();
            
            return View(data);
        }
        [HttpPost]
        public IActionResult Add(Color color)
        {
            var data = _colorService.AddColor(color);
            if (data)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(true, "Thất bại"));
            }
        }
        public IActionResult Delete(string id)
        {
            var isSuccess = _colorService.Remove(new Color() { Id = id } , true);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
    }
}
