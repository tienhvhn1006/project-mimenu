﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace CMS.MIGROUP.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData([FromQuery] FilterStringCustomer filter)
        {
            filter.AppId = currentAppId;
            int recordsTotal = 0;
            var result = _customerService.LoadData(filter, out recordsTotal);

            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = result });
        }

        public IActionResult Info(int id = 0)
        {
            Customer current = new Customer() { AppId = currentAppId };
            if (id > 0)
            {
                current = _customerService.FindById(id);
                if (current == null)
                {
                    return Redirect("/");
                }else if(current.AppId != currentAppId)
                {
                    return Redirect("/");
                }
            }
            return View(current);
        }
        [HttpPost]
        public IActionResult Add(Customer obj)
        {
            if (obj.Id <= 0)
            {
                obj.AppId = currentAppId;
                obj.DateCreated = DateTime.Now;
                obj.DateModified = DateTime.Now;

                obj.ActiveBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                obj.ModifiedBy = obj.ActiveBy;
            }
            else
            {
                obj.DateModified = DateTime.Now;
                obj.ModifiedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
            var isSuccess = _customerService.AddOrUpdate(obj);
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }
        }
    }
}

//Thông báo
//var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;
//request.KeepAlive = true;
//request.Method = "POST";
//request.ContentType = "application/json; charset=utf-8";
//request.Headers.Add("authorization", "Basic " + _configuration["OneSignal:AppSecret"]);
//string title = "Thông báo đơn hàng";
//string message = "Bạn có 1 đơn hàng mới ";
//string url = "https://cms.enterbuy.vn/";
//var obj = new
//{
//    app_id = _configuration["OneSignal:AppId"],
//    headings = new { en = title },
//    contents = new { en = message },
//    included_segments = new[] { "All" },
//    url
//};

//var param = JsonConvert.SerializeObject(obj);
//byte[] byteArray = Encoding.UTF8.GetBytes(param);

//string responseContent = null;

//try
//{
//    using (var writer = await request.GetRequestStreamAsync())
//    {
//        await writer.WriteAsync(byteArray, 0, byteArray.Length);
//    }

//    using (var response = await request.GetResponseAsync() as HttpWebResponse)
//    {
//        using (var reader = new StreamReader(response.GetResponseStream()))
//        {
//            responseContent = await reader.ReadToEndAsync();
//        }
//    }
//}
//catch (WebException ex)
//{
//    System.Diagnostics.Debug.WriteLine(ex.Message);
//    System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
//}