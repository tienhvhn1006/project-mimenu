﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class EnterprisesController : BaseController
    {
        IEnterprisesService _enterprisesService;
        IHelperUpload _helperUpload;
        public EnterprisesController(IEnterprisesService enterprisesService, IHelperUpload helperUpload)
        {
            _enterprisesService = enterprisesService;
            _helperUpload = helperUpload;
        }
        public IActionResult Index()
        {
            var lstData = _enterprisesService.FindAll().ToList();
            return View(lstData);
        }



        [HttpGet("/enterprises/info")]
        [HttpGet("/enterprises/info/{id}")]
        public IActionResult Info(string id = "")
        {
            Enterprise current = new Enterprise() { AppId = currentAppId };
            if (!String.IsNullOrEmpty(id))
            {
                current = _enterprisesService.FindById(id);
                if (current == null)
                {
                    return Redirect("/");
                }
            }
            return View(current);

        }
        [HttpPost]
        public IActionResult Add(Enterprise obj)
        {
            var lstFile = HttpContext.Request.Form.Files;
            if (lstFile != null && lstFile.Any())
            {
                obj.Avatar = _helperUpload.UploadImage("Files", currentAppId, lstFile.ToList());
            }

            obj.DateModified = DateTime.Now;
            obj.ModifiedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;


            var isSuccess = _enterprisesService.AddEnterprise(obj);
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }

        }
        public IActionResult Delete(string id)
        {
            var isSuccess = _enterprisesService.Remove(new Enterprise() { Id = id }, true);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
    }
}
