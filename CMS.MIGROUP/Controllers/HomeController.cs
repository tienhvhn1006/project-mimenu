﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CMS.MIGROUP.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using CORE.UTILITY;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.ViewModels;

namespace CMS.MIGROUP.Controllers
{

    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        //IOrderService _orderService;
        //IProductService _productService;

        public HomeController(IOrderService orderService, IProductService productService, ILogger<HomeController> logger)
        {
            //_orderService = orderService;
            //_productService = productService;
            _logger = logger;
        }

        
        public IActionResult Index()
        {

            //var homePageVM = new HomePageVM();
            //var date = DateTime.Today;
            //homePageVM.StatusOrder = _orderService.StatisticsByStatus(new DateTime(date.Year, date.Month, 1), DateTime.Now);
            //homePageVM.Top5Sale = BuildTop5(date);

            return View();
        }

        //private List<KeyValuePair<string, int>> BuildTop5(DateTime date)
        //{

        //    var top5 = _orderService.Top5Sale(new DateTime(date.Year, date.Month, 1), DateTime.Now);

        //    var nameP = _productService.GetNameById(top5.Keys.ToList());


        //    return top5.Select(x => new KeyValuePair<string, int>(nameP.ContainsKey(x.Key) ? nameP[x.Key] : $"Id: {x.Key} bị xóa", x.Value)).ToList();

        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
