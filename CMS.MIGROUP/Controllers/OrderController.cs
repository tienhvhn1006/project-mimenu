﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class OrderController : BaseController
    {
        IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        [HttpGet("order-{status}")]
        [HttpGet("order")]
        public IActionResult Index(StatusOrder status = StatusOrder.All)
        {
            ViewBag.Status = status;

            return View();
        }
        public ActionResult LoadData([FromQuery] FilterStringOrder filter)
        {
            int recordsTotal = 0;
            filter.AppId = currentAppId;
            var result = _orderService.LoadData(filter ,out recordsTotal);
            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = result });
        }

        public ActionResult Invoice(int orderId)
        {
            var order = _orderService.GetById(orderId);
            return PartialView("~/Views/Shared/Order/_UcInvoice.cshtml", order);
        }
        public IActionResult Delete(int id)
        {
            var isSuccess = _orderService.UpdateDeleted(id);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
        public IActionResult UpdateStatus(int id, StatusOrder status)
        {
            var item = new Order();
            item.Status = status;
            item.Id = id;
            var isSuccess = _orderService.UpdateStatus(item);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, UiHelper.ShowHtmlStatusOrder(status, id)));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
        [HttpGet("/thong-ke-don-{time}")]
        public IActionResult StatisticalSales(int time)
        {
            Dictionary<string, int> dicResult = new Dictionary<string, int>();
            var date = DateTime.Now;
            if (time == 1)
            {
                dicResult = _orderService.StatisticalByWeek(date,currentAppId);
            }
            if (time == 2)
            {
                dicResult = _orderService.StatisticalByMonth(date, currentAppId);
            }
            if (time == 3)
            {
                dicResult = _orderService.StatisticalByYear(date, currentAppId);
            }
            if (time == 4)
            {
                dicResult = _orderService.StatisticalByYear(new DateTime(date.Year-1,1,1), currentAppId);
            }
            return new ObjectResult(new KeyValuePair<string, int[]>(String.Join(",", dicResult.Keys.ToList()), dicResult.Values.ToArray()));
        }
        [HttpGet("/thong-ke-doanh-so-{time}")]
        public IActionResult StatisticalRevenue(int time)
        {
            Dictionary<string, double> dicResult = new Dictionary<string, double>();
            var date = DateTime.Now;
            if (time == 1)
            {
                dicResult = _orderService.StatisticalRevenueByWeek(date, currentAppId);
            }
            if (time == 2)
            {
                dicResult = _orderService.StatisticalRevenueByMonth(date, currentAppId);
            }
            if (time == 3)
            {
                dicResult = _orderService.StatisticalRevenueByYear(date, currentAppId);
            }
            if (time == 4)
            {
                dicResult = _orderService.StatisticalRevenueByYear(new DateTime(date.Year - 1, 1, 1), currentAppId);
            }
            return new ObjectResult(new KeyValuePair<string, double[]>(String.Join(",", dicResult.Keys.ToList()), dicResult.Values.ToArray()));
        }

        [HttpGet("/top5-{type}-{month}")]
        public IActionResult Top5(int type, int month)
        {
            List<KeyValuePair<string, string>> dicResult = new List<KeyValuePair<string, string>>();

            var date = DateTime.Now;
            if (type == 1)
            {
                dicResult = _orderService.Top5Sale(new DateTime(date.Year, month, 1), new DateTime(date.Year, month + 1, 1), currentAppId);
            }
            if (type == 2)
            {
                dicResult = _orderService.Top5Revenue(new DateTime(date.Year, month, 1), new DateTime(date.Year, month + 1, 1), currentAppId);
            }
            return PartialView("~/Views/Shared/Home/_UcTop5Sale.cshtml", dicResult);
        }

    }
}
