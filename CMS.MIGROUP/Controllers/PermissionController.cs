﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using CORE.UTILITY;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static CORE.UTILITY.SystemConstants;

namespace CMS.MIGROUP.Controllers
{

    public class PermissionController : BaseController
    {
        private readonly IAppRoleUserService _userAppRoleUser;
        private readonly IAppUserService _userApp;

        private readonly UserManager<AppUser> _userManager;
        private readonly IAppRoleService _appRoleService;

        private readonly IRolePermissionService _rolePermissionService;
        public PermissionController(IAppUserService userApp,
            UserManager<AppUser> userManager,
            IAppRoleUserService userAppRoleUser,
            IAppRoleService appRoleService,
            IRolePermissionService rolePermissionService)
        {
            _userApp = userApp;
            _userManager = userManager;
            _userAppRoleUser = userAppRoleUser;
            _appRoleService = appRoleService;
            _rolePermissionService = rolePermissionService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData([FromQuery] FilterString filter)
        {
            int recordsTotal = 0;
            var result = _userApp.LoadData(filter, out recordsTotal);
            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = result });
        }

        public IActionResult Info(string id = "")
        {
            AppUser current = new AppUser();
            if (!String.IsNullOrEmpty(id))
            {
                var appUser = _userApp.FindAll(x => x.UserName == id).ToList();
                if (appUser == null)
                {
                    return RedirectToAction("Index", "Permission");
                }
                else
                {
                    current = appUser.FirstOrDefault();
                }
            }
            return View(current);
        }
        [HttpPost]
        public async Task<IActionResult> Add(AppUserVM product)
        {
            var isSuccess = false;
            var json = JsonConvert.SerializeObject(product);
            if (product.Id != null && product.Id != Guid.Empty)
            {
                var appUser = JsonConvert.DeserializeObject<AppUser>(json);
                appUser.DateModified = DateTime.Now;
                isSuccess = _userApp.UpdateInfo(appUser, product.Roles);
            }
            else
            {
                var appUser = JsonConvert.DeserializeObject<AppUser>(json);
                appUser.Id = Guid.NewGuid();
                appUser.DateModified = DateTime.Now;
                appUser.DateCreated = DateTime.Now;
                var result = await _userManager.CreateAsync(appUser, product.PasswordHash);
                if (result.Succeeded)
                {
                    if (product.Roles != null && product.Roles.Any())
                    {
                        bool isA = _userAppRoleUser.AddMutil(product.Roles, appUser.Id);
                    }
                    isSuccess = true;
                }
                else
                {
                    return new ObjectResult(new GenericResult(false, result.Errors.FirstOrDefault().Description));
                }

            }
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }
        }

        public IActionResult Roles()
        {
            var data = _appRoleService.FindAll().ToList();
            return View(data);
        }
        [HttpPost]
        public IActionResult AddRole(AppRole app)
        {
            var isSuccess = false;
            if (app.Id != null && app.Id != Guid.Empty)
            {
                app.NormalizedName = app.Name.ToLower();
                app.ConcurrencyStamp = MUtility.TimeEpochNow();
                isSuccess = _appRoleService.Update(app, true);
            }
            else
            {
                app.NormalizedName = app.Name.ToLower();
                app.ConcurrencyStamp = MUtility.TimeEpochNow();
                isSuccess = _appRoleService.Add(app, true);
            }
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }
        }

        public async Task<IActionResult> UpdateStatus(string id, Status status)
        {
            var item = await _userManager.FindByNameAsync(id);
            if (item != null)
            {
                item.Status = status;
                item.DateModified = DateTime.Now;
                var isSuccess = _userApp.UnLock(item);
                if (isSuccess)
                    return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
                else
                    return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Không tìm thấy tài khoản"));
            }

        }
        public IActionResult DeleteRole(Guid id)
        {
            var isSuccess = _appRoleService.DeleteRole(id);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }


        public IActionResult UserPermission(Guid Id)
        {
            var data = _rolePermissionService.FindAll(x => x.RoleId == Id).ToList();
            var VM = data.Select(x => new Actions() { FunctionId = x.FunctionId, Action = x.Action, IsEdit = true }).ToList();
            return View(new PermissionVM() { Id = Id, Actions = VM });
        }
        [HttpGet("/doi-mat-khau")]
        public IActionResult ChangePass()
        {

            return View();
        }
        [HttpPost("/change-pass")]
        public async Task<IActionResult> ChangePass(ChangePassVM obj)
        {
            bool isSuccess = false;
            var userName = this.User.FindFirst(ClaimTypes.Name).Value;

            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                var res = _userManager.ChangePasswordAsync(user, obj.PassOld, obj.PassNew);
                isSuccess = res.Result.Succeeded;
            }

            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Thông tin chưa phù hợp"));
        }
    }
}
