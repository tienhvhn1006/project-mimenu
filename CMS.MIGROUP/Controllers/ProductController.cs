﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;

using CORE.DATA.Entities;
using CORE.DATA.Enums;
using CORE.UTILITY;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class ProductController : BaseController
    {
        IProductService _productService;
        IHelperUpload _helperUpload;
        public ProductController(
              IProductService productService,
              IHelperUpload helperUpload

            )
        {
            _productService = productService;
            _helperUpload = helperUpload;

        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData([FromQuery] Filter filter)
        {
            filter.AppId = currentAppId;
            int recordsTotal = 0;
            var result = _productService.LoadData(filter, TypeContent.Product, out recordsTotal);

            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = result });
        }
        public IActionResult Delete(int id)
        {
            var isSuccess = _productService.Delete(id);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }

        public IActionResult Info(int id = 0)
        {
            Product current = new Product() { Type = TypeContent.Product, TypePost = TypeNews.Product, Code = MUtility.GenCode("SP"), AppId = currentAppId };
            if (id > 0)
            {
                current = _productService.FindById(id);
                if (current == null)
                {
                    return Redirect("/");
                }
            }
            if (current.TypePost == TypeNews.All)
            {
                current.TypePost = TypeNews.Product;
            }
            return View(current);
            //else
            //    return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
        [HttpPost]
        public IActionResult Add(TypeProductVM product)
        {
            var lstFile = HttpContext.Request.Form.Files;
            if (lstFile != null && lstFile.Any())
            {
                product.Image = _helperUpload.UploadImage("Files", currentAppId, lstFile.ToList());
            }

            if (product.Id <= 0)
            {
                product.AppId = currentAppId;
                product.DateCreated = DateTime.Now;
                product.DateModified = DateTime.Now;
                product.DatePublished = DateTime.Now;
                product.CreatedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                product.ModifiedBy = product.CreatedBy;
            }
            else
            {
                product.DateModified = DateTime.Now;
                product.ModifiedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
            var Obj = product.Init();

            var isSuccess = _productService.AddProduct(Obj);
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi xảy ra"));
            }
        }
        public IActionResult UpdateStatus(int id, Status status)
        {
            var item = new Product();
            item.Id = id;
            item.Status = status;
            item.DateModified = DateTime.Now;
            var isSuccess = _productService.UnLock(item);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }

    }
}
