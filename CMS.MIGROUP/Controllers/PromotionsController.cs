﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class PromotionsController : BaseController
    {
        IPromotionService _promotionService;
        public PromotionsController(IPromotionService promotionService)
        {
            _promotionService = promotionService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult LoadData([FromQuery] FilterString filter)
        {
            filter.AppId = currentAppId;
            int recordsTotal = 0;
            var result = _promotionService.LoadData(filter, out recordsTotal);

            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = result });
        }

        public IActionResult Info(string id = "")
        {
            Promotion current = new Promotion() { AppId = currentAppId };
            if (!String.IsNullOrEmpty(id))
            {
                current = _promotionService.FindById(id);
                if (current == null)
                {
                    return Redirect("/");
                }
            }
            return View(current);

        }
        [HttpPost]
        public IActionResult Add(Promotion obj)
        {
            if (String.IsNullOrEmpty(obj.Id))
            {
                obj.AppId = currentAppId;
                obj.DateCreated = DateTime.Now;
                obj.DateModified = DateTime.Now;
                obj.CreatedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                obj.ModifiedBy = obj.CreatedBy;
            }
            else
            {
                obj.DateModified = DateTime.Now;
                obj.ModifiedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }

            var isSuccess = _promotionService.AddPromotion(obj);
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }
        }
        public IActionResult Delete(string id)
        {
            var isSuccess = _promotionService.Remove(new Promotion() { Id = id }, true);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }
    }
}
