﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;

namespace CMS.MIGROUP.Controllers
{
    public class PropertiesController : Controller
    {
        IPropertiesService _propertiesService;
        public PropertiesController(IPropertiesService propertiesService)
        {
            _propertiesService = propertiesService;
        }
        public IActionResult Index()
        {
            var data = _propertiesService.FindAll().ToList();
            return View(data);
        }
        [HttpPost]
        public IActionResult Add(Property property)
        {
            var data = _propertiesService.AddProperty(property);
            if (data)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(true, "Thất bại"));
            }
        }
        public IActionResult Delete(int id)
        {
            var isSuccess = _propertiesService.Remove(id, true);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }


    }
}
