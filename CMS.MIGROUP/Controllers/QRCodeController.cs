﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using CORE.UTILITY;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using SkiaSharp;
using SkiaSharp.System.Drawing;
using Spire.Barcode;
using static CORE.UTILITY.SystemConstants;

namespace CMS.MIGROUP.Controllers
{
    public class QRCodeController : BaseController
    {
        IQRCodeService _qRCodeService;
        IHostingEnvironment _env;
        public QRCodeController(IQRCodeService qRCodeService, IHostingEnvironment env)
        {
            _qRCodeService = qRCodeService;
            _env = env;
        }
        public IActionResult Index()
        {
            var QRCode = _qRCodeService.FindAll(x => x.AppId == currentAppId).ToList();
            return View(QRCode);
        }
        public IActionResult Info(int id = 0)
        {

            QRCode current = new QRCode() { AppId = currentAppId };
            if (id > 0)
            {
                current = _qRCodeService.FindById(id);
                if (current == null)
                {
                    return Redirect("/");
                }
            }
            return View(current);

        }
        [HttpPost]
        public IActionResult Add(QRCode obj, bool isCheckAuto = true)
        {
            //bool isCheckAuto = true;
            bool result = false;
            var domain = this.User.FindFirst(UserClaim.Domain).Value;
            if (!String.IsNullOrEmpty(domain))
            {
                if (obj.Id > 0)
                {
                    var isCheck = _qRCodeService.IsExist(x => x.Id == obj.Id && x.Url != obj.Url);
                    if (isCheck || String.IsNullOrEmpty(obj.Url))
                    {
                        string url = MUtility.UrlOrder(obj.Id, domain);
                        if (!String.IsNullOrEmpty(obj.Url))
                        {
                            url = obj.Url;
                        }
                        obj.Image = GenAutoQRCode(url, obj.Id);
                        obj.Url = url;
                    }
                    result = _qRCodeService.Update(obj, true);
                }
                else
                {
                    var isSucces = _qRCodeService.Add(obj, true);
                    if (isSucces)
                    {
                        if (isCheckAuto)
                        {
                            string url = MUtility.UrlOrder(obj.Id, domain);
                            obj.Image = GenAutoQRCode(url, obj.Id);
                            obj.Url = url;
                        }
                        else
                        {

                            obj.Image = GenAutoQRCode(obj.Url, obj.Id);
                        }
                        result = _qRCodeService.Update(obj, true);
                    }
                    else
                    {
                        result = false;
                    }
                }
                if (result)
                {
                    return new ObjectResult(new GenericResult(true, "Thành công"));
                }
                else
                {
                    return new ObjectResult(new GenericResult(false, "Thất bại"));
                }

            }
            else
            {
                return new ObjectResult(new GenericResult(true, "Tài khoản chưa được liên kết Website"));
            }
        }
        public IActionResult Delete(int id)
        {
            var isSuccess = _qRCodeService.Remove(id, true);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }

        public string GenAutoQRCode(string url, int id)
        {
            BarcodeSettings settings = new BarcodeSettings();
            settings.Type = BarCodeType.QRCode;
            settings.Unit = SkiaSharp.System.Drawing.GraphicsUnit.Pixel;
            settings.ShowText = false;
            settings.ResolutionType = ResolutionType.UseDpi;
            settings.Data = url;

            settings.X = 150;
            settings.Y = 150;
            settings.LeftMargin = 5;
            settings.RightMargin = 5;
            settings.TopMargin = 5;
            settings.BottomMargin = 5;

            //generate QR code  
            BarCodeGenerator generator = new BarCodeGenerator(settings);
            SkiaSharp.SKImage QRbarcode = generator.GenerateImage();
            //display QR code image in picture box  
            //pictureBox1.Image = QRbarcode;  

            string pathRoot = "Files";
            //QRbarcode.savege("barcode.png");
            SKData encoded = QRbarcode.Encode();

            var webRoot = _env.WebRootPath;
            string fileName = "";

            var pathToSave = Path.Combine(webRoot, pathRoot);

            using (Stream stream = encoded.AsStream())
            {
                fileName = Path.GetFileName(pathRoot + "\\" + id + "_" + DateTime.Now.ToString("dd-MM-yyyyHHmmss") + "_QRCode.png");
                var fullPath = Path.Combine(pathToSave, fileName);
                var uploadedImage = Image.FromStream(stream);
                Bitmap origBMP = new Bitmap(uploadedImage);
                origBMP.Save(fullPath);

                var thumbPathToSave = Path.Combine("Files", pathToSave, ".tmb");
                if (!Directory.Exists(thumbPathToSave))
                {
                    Directory.CreateDirectory(thumbPathToSave);
                }
                var thumbPath = Path.Combine(thumbPathToSave, fileName);
                using (var stream1 = encoded.AsStream())
                {
                    uploadedImage = Image.FromStream(stream1);

                    origBMP = new Bitmap(uploadedImage);
                    if (origBMP.Width <= 150)
                    {
                        origBMP.Save(thumbPath);
                    }
                    else
                    {
                        int origWidth = origBMP.Width;
                        int origHeight = origBMP.Height;
                        int newWidth = 150;
                        int newHeight = newWidth * origHeight / origWidth;

                        Bitmap newBMP = new Bitmap(origBMP, newWidth, newHeight);
                        Graphics objGra = Graphics.FromImage(newBMP);
                        objGra.DrawImage(origBMP, new Rectangle(0, 0, newBMP.Width, newBMP.Height), 0, 0, origWidth, origHeight, System.Drawing.GraphicsUnit.Pixel);
                        objGra.Dispose();
                        newBMP.Save(thumbPath);
                    }
                    //file.CopyTo(stream1);
                }
            }
            return "/" + fileName;
        }
    }
}


