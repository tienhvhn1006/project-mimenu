﻿using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;

using CORE.UTILITY;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CORE.APPLICATION.Cache;
using CORE.APPLICATION.Setup;
using CORE.DATA.Enums;
using CORE.DATA.Entities;
using System.Security.Claims;

namespace CMS.MIGROUP.Controllers
{
    public class ZoneController : BaseController
    {
        IProductService _productService;
        public ZoneController(
              IProductService productService
            )
        {
            _productService = productService;
        }

        [HttpGet("/danh-muc-{type}")]
        //[HttpGet("/danh-muc-bai-viet")]
        public IActionResult Index(TypeNews type = TypeNews.Blog)
        {
            ViewBag.Url = "/danh-muc-" + (int)type + "/info";
            var Zones = _productService.FindAll(x => x.Type == TypeContent.Zone && x.AppId == currentAppId && x.TypePost == type, new string[] { "Id", "Name", "Status", "ParentId" }).ToList();

            var TreeModel = Util.GetTree(Zones, 0);
            //var O = TreeModel.Where(x => x.Child.Count > 0).ToList();
            return View(TreeModel);
        }
        [HttpGet("/danh-muc-{type}/info")]
        public IActionResult Info(int id = 0, TypeNews type = TypeNews.Blog)
        {
            ViewBag.Url = "/danh-muc-" + (int)type;
            Product current = new Product() { Type = TypeContent.Zone, TypePost = type, Code = MUtility.GenCode("ZO"), AppId = currentAppId };
            if (id > 0)
            {
                current = _productService.FindById(id);
                if (current == null)
                {
                    current = new Product() { Type = TypeContent.Zone, TypePost = type, Code = MUtility.GenCode("ZO"), AppId = currentAppId };
                }
            }
            return View(current);
        }
        public IActionResult Delete(int id)
        {
            var isSuccess = _productService.DeleteZone(id);
            if (isSuccess)
                return new ObjectResult(new GenericResult(isSuccess, "Thành công"));
            else
                return new ObjectResult(new GenericResult(isSuccess, "Có lỗi trong quá trình xóa dữ liệu"));
        }

        [HttpPost("/danh-muc-1/add")]
        [HttpPost("/danh-muc-2/add")]
        [HttpPost("/danh-muc-3/add")]
        [HttpPost("/danh-muc-4/add")]
        public IActionResult Add(Product product)
        {

            if (product.Id <= 0)
            {
                product.AppId = currentAppId;
                product.DateCreated = DateTime.Now;
                product.DateModified = DateTime.Now;
                product.DatePublished = DateTime.Now;
                product.CreatedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                product.ModifiedBy = product.CreatedBy;
            }
            else
            {
                product.DateModified = DateTime.Now;
                product.ModifiedBy = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
            product.SetDefault();
            var isSuccess = _productService.AddProduct(product);
            if (isSuccess)
            {
                return new ObjectResult(new GenericResult(true, "Thành công"));
            }
            else
            {
                return new ObjectResult(new GenericResult(false, "Có lỗi trong quá trình xóa dữ liệu"));
            }
        }
    }
}
