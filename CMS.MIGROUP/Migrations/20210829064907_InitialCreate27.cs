﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CMS.CORE.Migrations
{
    public partial class InitialCreate27 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AppId",
                table: "AppUsers",
                newName: "EnterpriseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EnterpriseId",
                table: "AppUsers",
                newName: "AppId");
        }
    }
}
