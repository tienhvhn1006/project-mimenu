﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CMS.CORE.Migrations
{
    public partial class InitialCreate29 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnterpriseId",
                table: "AppUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EnterpriseId",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
