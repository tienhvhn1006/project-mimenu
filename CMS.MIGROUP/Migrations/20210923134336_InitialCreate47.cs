﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CMS.CORE.Migrations
{
    public partial class InitialCreate47 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Avatar2",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FaceBook",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Linkedin",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Twiter",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Youtube",
                table: "Enterprises",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "Avatar2",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "FaceBook",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "Linkedin",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "Twiter",
                table: "Enterprises");

            migrationBuilder.DropColumn(
                name: "Youtube",
                table: "Enterprises");
        }
    }
}
