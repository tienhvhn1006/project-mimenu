﻿
$(document).ready(function () {
    "use strict"

    function getUrl() {
        return "/customer/loaddata"
    }

    // init thumb view datatable

    var lstObj = [
        {
            title: "Mã KH",
            sortable: true,
            class: " col-sm-1 col-xs-12 dt-checkboxes-cell text-center",
            'render': function (data, type, full, meta) {
                return '<span class="pl-1">' + full.id + '</span> ';
            }
        },
        
    ];

    $("th[x-field=true]").loadField(lstObj);

    $(".data-thumb-view").serverDataTable(getUrl(), lstObj, function () {
        location.href = "/customer/info";
    });

    $(document).ajaxComplete(function () {
        deleteDataTable("/customer/delete");
        editDataTable("/customer/info");
    });

    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }
})
