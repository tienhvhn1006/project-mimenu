﻿/*=========================================================================================
    File Name: chart-apex.js
    Description: Apexchart Examples
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function () {

    var $primary = '#7367F0',
        $success = '#28C76F',
        $danger = '#EA5455',
        $warning = '#FF9F43',
        $info = '#00cfe8',
        $label_color_light = '#dae1e7';

    var themeColors = [$primary, $success, $danger, $warning, $info];

    // RTL Support
    var yaxis_opposite = false;
    if ($('html').data('textdirection') == 'rtl') {
        yaxis_opposite = true;
    }

    //Apex.tooltip = {
    //    custom: function ({ series, seriesIndex, dataPointIndex, w }) {
    //        return (
    //            '<div class="apexcharts-tooltip light">' +
    //            '<span> +
    //            w.globals.labels[dataPointIndex] +
    //            ": " +
    //            formatCurrencyseries[seriesIndex][dataPointIndex] +
    //            "</span>" +
    //            "</div>"
    //        );
    //    }
    //}

    // Bar Chart
    // ----------------------------------
    var barChartOptions = {
        chart: {
            height: 550,
            type: 'bar',
        },
        colors: themeColors,
        plotOptions: {
            bar: {
                horizontal: true,
            }
        },
        dataLabels: {
            enabled: false
        },
        series: [{
            data: dataChartSales,
            name: "Doanh số"
        }],
        xaxis: {
            categories: dataTxtChartSales.split(','),
            tickAmount: 5
        },
        yaxis: {
            opposite: yaxis_opposite
        }
    }

    var barChart = new ApexCharts(
        document.querySelector("#bar-chart"),
        barChartOptions
    );
    barChart.render();


    $("#drop-date-sales .dropdown-item").on("click", function (e) {
        e.preventDefault();

        $(this).closest("#drop-date-sales").find("button").text(" " + $(this).text() + " ");

        var url = $(this).data("href");
        $.get(url, function (data) {
            dataTxtChartSales = data.key;
            dataChartSales = data.value;

            barChartOptions.series[0].data = dataChartSales;
            barChartOptions.xaxis.categories = dataTxtChartSales.split(',');
            $("#bar-chart").html("");

            var barChart = new ApexCharts(
                document.querySelector("#bar-chart"),
                barChartOptions
            );
            barChart.render();
        })

    });



    var barChartOptions_1 = {
        chart: {
            height: 550,
            type: 'bar',
        },
        colors: themeColors,
        plotOptions: {
            bar: {
                horizontal: true,
            }
        },
        dataLabels: {
            enabled: false
        },
        series: [
            {
                data: dataChartRevenue,
                name: "Doanh thu"
            }
        ],
        xaxis: {
            categories: dataTxtChartRevenue.split(','),
            tickAmount: 5,
            labels: {
                formatter: function (value) {
                    var val = Math.abs(value)
                    if (val >= 1000000) {
                        val = (val / 1000000).toFixed(0) + ' Tr'
                    } else if (val >= 1000) {
                        val = (val / 1000).toFixed(0) + ' K'
                    }
                    return val
                }
            },

        },
        tooltip: {
            x: {
                show: true
            },
            y: {
                formatter: function (value) {
                    return formatCurrencyNumber(value);
                }
            }
        },
        yaxis: {
            opposite: yaxis_opposite
        }
    }
    var barChart1 = new ApexCharts(
        document.querySelector("#bar-chart-1"),
        barChartOptions_1
    );
    barChart1.render();

    $("#drop-date-revenue .dropdown-item").on("click", function (e) {
        e.preventDefault();

        $(this).closest("#drop-date-revenue").find("button").text(" " + $(this).text() + " ");

        var url = $(this).data("href");
        $.get(url, function (data) {
            dataChartRevenue = data.value;
            dataTxtChartRevenue = data.key;
            barChartOptions_1.series[0].data = dataChartRevenue;
            barChartOptions_1.xaxis.categories = dataTxtChartRevenue.split(',');
            $("#bar-chart-1").html("");

            var barChart = new ApexCharts(
                document.querySelector("#bar-chart-1"),
                barChartOptions_1
            );
            barChart.render();
        })

    });
});
