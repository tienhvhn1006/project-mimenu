﻿
$("#drop-top-5 #data-type .dropdown-item").on("click", function (e) {
    e.preventDefault();
    $(this).closest("#data-type").find("button").attr("data-type", $(this).data("href")).text(" " + $(this).text() + " ");
    loadTop();
});

$("#drop-top-5 #data-date .dropdown-item").on("click", function (e) {
    e.preventDefault();
    $(this).closest("#data-date").find("button").attr("data-date", $(this).data("href")).text(" " + $(this).text() + " ");
    loadTop();
});

function loadTop() {
    var parrent = $("#drop-top-5");
    var type = parrent.find("[data-type]").attr("data-type");
    var date = parrent.find("[data-date]").attr("data-date");
    $.get("/top5-" + type + "-" + date + "", function (data) {
        $("#top-product").html(data);
    });
}