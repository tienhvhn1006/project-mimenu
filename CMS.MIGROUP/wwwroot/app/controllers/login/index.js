﻿var loginController = function () {
    this.initialize = function () {
        registerEvents();
    }

    var registerEvents = function () {
        $('#login').submit(function (e) {
            e.preventDefault();

            var $this = $(this);
            login($this);
        });
    }

    var login = function (ele) {
        $.ajax({
            type: $(ele).attr("method"),
            data: $(ele).serialize(),
            dateType: 'json',
            url: $(ele).attr("action"),
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
               
                if (res.success) {
                    common.stopLoading();
                    window.location.href = "/";
                }
                else {
                    common.stopLoading();
                    toastr.error(res.message, 'Có lỗi');
                }
            }
        })
    }
}
var forgetController = function () {
    this.initialize = function () {
        registerEvents();
    }

    var registerEvents = function () {
        $('#forgetPass').submit(function (e) {
            e.preventDefault();

            var $this = $(this);
            forget($this);
        });
    }

    var forget = function (ele) {

        $.ajax({
            type: $(ele).attr("method"),
            data: {
                email: $("#Email").val()
            },
            dateType: 'json',
            url: $(ele).attr("action"),
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {

                if (res.success) {
                    common.stopLoading();
                    toastr.success(res.message, 'Thành công');
                }
                else {
                    common.stopLoading();
                    toastr.error(res.message, 'Có lỗi');
                }
            }
        })
    }
}
var resetController = function () {
    this.initialize = function () {
        registerEvents();
    }
    var registerEvents = function () {
        $('#reset-pass').submit(function (e) {
            e.preventDefault();
            var $this = $(this);
            reset($this);
        });
    }
    var reset = function (ele) {
        $.ajax({
            type: $(ele).attr("method"),
            data: $(ele).serialize() ,
            dateType: 'json',
            url: $(ele).attr("action"),
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
                
                if (res.success) {
                    common.stopLoading();
                    toastr.success(res.message, 'Thành công');
                }
                else {
                    common.stopLoading();
                    toastr.error(res.message, 'Có lỗi');
                }
            }
        })
    }
}


var common = {

    startLoading: function () {
        $(".overlay").fadeIn("fast", function () {
            $("body").addClass("loading");
        });
    },
    stopLoading: function () {
        $(".overlay").fadeOut("fast", function () {
            $("body").removeClass("loading");
        });
    }
};