﻿
$(document).ready(function () {
    "use strict"

    AddHeight();

    function getUrl() {
        return "/order/loaddata?status=" + $("input[name='status']").val();
    }
    // init thumb view datatable
    var lstObj = [
        {
            title: "Trạng thái",
            sortable: false,
            class: " col-md-1 col-xs-12 dt-checkboxes-cell text-center",
            'render': function (data, type, full, meta) {
                return '<div class="clickBtn">+</div><span>' + full.status + '<span/>';
            }
        },
    ];

    $("th[x-field=true]").loadField(lstObj);

    $(".data-thumb-view").serverDataTable(getUrl(), lstObj, function () {
        location.href = "/order/info";
    });

    $(document).ajaxComplete(function () {
        deleteDataTable("/order/delete");
        editDataTable("/order/info");
        viewDataTable();
    });
    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }
    $("#mSort").on("change", function () {
        $('.data-thumb-view').DataTable().ajax.url(getUrl()).load();
    });

    $(document).on("click", "#print-order", function () {

        var temp = $("#order-print").html();
        PrintElem(temp);
    });

})

var viewDataTable = function () {
    $(document).on("click", ".action-view", function (e) {
        e.preventDefault();
        var $this = $(this);

        let Id = $this.attr("data-id");
        $.ajax({
            url: "/order/invoice",
            method: "Get",
            data: {
                orderId: Id
            },
            beforeSend: function () {
                common.startLoading();
            },
        }).done(function (res) {
            common.stopLoading();
            $("#main_layout").html(res);
            $(".add-new-data").addClass("show");
            $(".overlay-bg").addClass("show");
            AddHeight();
        });
    });
}
//$(document).on("click", "#updateStatus", function (e) {
//    e.preventDefault();
//    var id = $(this).data("id");
//    var status = $("#status").val();
//    let urlResquest = "/order/updatestatus?id=" + id + "&status=" + status;
//    $.get(urlResquest, function (result) {
//        if (result.success) {
//            $("#stod-" + id + "").html(result.message);
//            toastr.success("Thành công", 'Thay đổi trạng thái ');
//        }
//        else
//            toastr.error(result.message, 'Thay đổi trạng thái');
//    })
//});
$("#status").on("change", function () {
    var id = $("OrderId").val();
    var status = $(this).val();
    let urlResquest = "/order/updatestatus?id=" + id + "&status=" + status;
    $.get(urlResquest, function (result) {
        if (result.success) {
            $("#stod-" + id + "").html(result.message);
            toastr.success("Thành công", 'Thay đổi trạng thái ');
        }
        else
            toastr.error(result.message, 'Thay đổi trạng thái');
    })

});
function AddHeight() {

    var height = window.innerHeight;
    var scroll = $(".order-scoll").innerHeight();

    if (scroll > height - 150) {
        $(".order-scoll").css("height", height - 150 + "px");
    }

}


function PrintElem(elem) {
    debugger
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title + '</h1>');
    mywindow.document.write(elem);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}