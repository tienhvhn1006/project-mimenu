﻿let path = "";
function getUrl() {
    return path + "/loaddata?sort=" + $("#mSort").val() + "&sortDir=" + $("input[name=SortDir]:checked", "#data-thumb-view").val();
}

// init thumb view datatable
function InitData() {
    var lstObj = [
        {
            title: "Id",
            sortable: false,
            class: " col-sm-1 col-xs-12 dt-checkboxes-cell text-center",
            'render': function (data, type, full, meta) {
                //return '<input type="checkbox" class="dt-checkboxes" name="id[]" value="' + full.id + '"/> <span class="pl-1">' + full.id + '</span> ';
                                return '<span class="pl-1">' + full.id + '</span> ';
            }
        },
    ];
    $("th[x-field=true]").loadField(lstObj);
    $(".data-thumb-view").serverDataTable(getUrl(), lstObj, function () {
        location.href = path + "/info";
    });
    $('#data-thumb-view input[name="SortDir"]').on("change", function () {
        debugger
        $('.data-thumb-view').DataTable().ajax.url(getUrl()).load();
    });
    $('#data-thumb-view #mSort').on("change", function () {
        $('.data-thumb-view').DataTable().ajax.url(getUrl()).load();
    });

    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }
    $(document).ajaxComplete(function () {
        deleteDataTable(path + "/delete");
        editDataTable(path + "/info");
        unlockDataTable("product/updatestatus");
        lockDataTable("product/updatestatus");
    });


    $("#mSort").on("change", function () {
        $('.data-thumb-view').DataTable().ajax.url(getUrl()).load();
    });

    $("input[name=SortDir]").on("change", function () {
        $('.data-thumb-view').DataTable().ajax.url(getUrl()).load();
    });
}





