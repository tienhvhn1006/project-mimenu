﻿
$(document).ready(function () {
    "use strict"

    function getUrl() {
        return "/recruitment/loaddata"
    }

    // init thumb view datatable

    var lstObj = [
        {
            title: "Id",
            sortable: false,
            class: " col-sm-1 col-xs-12 dt-checkboxes-cell",
            'render': function (data, type, full, meta) {
                return '<input type="checkbox" class="dt-checkboxes" name="id[]" value="' + full.id + '"/> <span class="pl-1">' + full.id + '</span> ';
            }
        },
    ];

    $("th[x-field=true]").loadField(lstObj);

    $(".data-thumb-view").serverDataTable(getUrl(), lstObj, function () {
        location.href = "/recruitment/info";
    });

    $(document).ajaxComplete(function () {
        deleteDataTable("/recruitment/delete");
        editDataTable("/recruitment/info");
    });

    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }
})
