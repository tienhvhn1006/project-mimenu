﻿
$(document).ready(function () {
    "use strict"
    var path = "permission";
    function getUrl() {
        return "/" + path + "/loaddata"
    }

    // init thumb view datatable

    var lstObj = [
        {
            title: "",
            sortable: false,
            class: " col-sm-1 col-xs-12 dt-checkboxes-cell",
            'render': function (data, type, full, meta) {
                return '<input type="checkbox" class="dt-checkboxes" name="id[]" value="' + full.id + '"/>  ';
            }
        },
        //{ data: "name", name: "name", title: "Tên sản phẩm", "autoWidth": true },
        //{ data: "category", title: "Danh mục", name: "category", class: "col-sm-2 col-xs-12" },
        //{ data: "date", title: "Ngày tháng", name: "date", class: "col-sm-2 col-xs-12", },
        //{ data: "status", title: "Trạng thái", name: "status", class: "col-sm-1 col-xs-12", },
        //{ data: "action", title: "Thao tác", name: "action", class: "col-sm-1 col-xs-12", },
    ];

    $("th[x-field=true]").loadField(lstObj);

    $(".data-thumb-view").serverDataTable(getUrl(), lstObj, function () {
        location.href = "/" + path + "/info";
    });

    $(document).ajaxComplete(function () {
        deleteDataTable_S("/" + path + "/delete");
        editDataTable("/" + path + "/info");
        unlockDataTable("/" + path + "/updatestatus");
        lockDataTable("/" + path + "/updatestatus");
    });

    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }
})
