﻿
$.fn.serverDataTable = function (path, column, fnaction) {
    // init thumb view datatable
    let urlResquest = path;
    var dataThumbView = $(this).DataTable({
        responsive: true,

        bProcessing: true,
        bServerSide: true,
        searchDelay: 1000,
        paging: true,
        sPaginationType: 'full_numbers',
        columnDefs: [
            {
                orderable: true,
                //targets: 0,
                //checkboxes: { selectRow: false }
            }
        ],
        sAjaxSource: urlResquest,
        columns: column,
        dom:
            '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        select: {
            style: "multi"
        },
        order: [[0, "desc"]],
        bInfo: false,
        pageLength: 10,
        buttons: [
            {
                text: "<i class='feather icon-plus'></i> Thêm mới",
                action: fnaction,
                className: "btn-outline-primary"
            }
        ],
        initComplete: function (settings, json) {
            $(".dt-buttons .btn").removeClass("btn-secondary")
        }
    })
    var actionDropdown = $(".actions-select");
    actionDropdown.insertAfter($(".top .actions .dt-buttons"));


    dataThumbView.on('draw.dt', function () {
        setTimeout(function () {
            if (navigator.userAgent.indexOf("Mac OS X") != -1) {
                $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
            }
        }, 50);
    });
}
$.fn.ClientDataTable = function (fnaction) {
    var dataListView = $(this).DataTable({
        responsive: false,
        columnDefs: [
            {
                orderable: true,
                targets: 0,
                checkboxes: { selectRow: true }
            }
        ],
        dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        select: {
            style: "multi"
        },
        order: [[0, "asc"]],
        bInfo: false,
        pageLength: 10,

        buttons: [
            {
                text: "<i class='feather icon-plus'></i> Thêm mới",
                action: fnaction,
                className: "btn-outline-primary"
            }
        ],
        initComplete: function (settings, json) {
            $(".dt-buttons .btn").removeClass("btn-secondary")
        }
    });
}

var isAction = false;

var deleteDataTable = function (path) {
    $(document).on("click", ".action-delete", function (e) {

        e.preventDefault();
        if (!isAction) {
            isAction = true;
            setTimeout(function () {
                isAction = false;
            }, 1000);
            //timeDelete = true;
            var $this = $(this)
            let urlResquest = path;
            debugger
            let Id = $this.attr("data-id");
            toastr.info("<br /><button type='button' class='btn btn-danger' value='yes'>Yes</button><button class='btn btn-default' type='button' value='no' >No</button>", 'Bạn có chắc chắn muốn xóa bản ghi ' + Id + ' ?',
                {
                    closeButton: false,
                    allowHtml: true,
                    onclick: function (toast) {
                        value = toast.target.value;

                        if (value == 'yes') {
                            debugger

                            urlResquest = urlResquest + "?id=" + Id;
                            $.get(urlResquest, function (result) {
                                if (result.success) {
                                    $this.closest('td').parent('tr').fadeOut();
                                    toastr.success(result.message, 'Xóa dữ liệu');
                                }
                                else
                                    toastr.error(result.message, 'Xóa dữ liệu');
                            });
                        } else {
                            toastr.remove()
                        }
                    }
                });
        }

    });
}
var editDataTable = function (path) {
    $(document).on("click", ".action-edit", function (e) {

        e.preventDefault();
        if (!isAction) {
            isAction = true;
            setTimeout(function () {
                isAction = false;
            }, 1000);
            //timeDelete = true;
            var $this = $(this)
            let urlResquest = path;
            let Id = $this.attr("data-id");
            urlResquest = urlResquest + "?id=" + Id;
            location.href = urlResquest;
        }

    });
}

var lockDataTable = function (path) {
    $(document).on("click", ".action-lock", function (e) {

        e.preventDefault();
        if (!isAction) {
            isAction = true;
            setTimeout(function () {
                isAction = false;
            }, 1000);
            //timeDelete = true;
            var $this = $(this)
            let urlResquest = path;
            debugger
            let Id = $this.attr("data-id");
            toastr.info("<br /><button type='button' class='btn btn-danger' value='yes'>Yes</button><button class='btn btn-default' type='button' value='no' >No</button>", 'Bạn có chắc chắn muốn khóa bản ghi ' + Id + ' ?',
                {
                    closeButton: false,
                    allowHtml: true,
                    onclick: function (toast) {
                        value = toast.target.value;

                        if (value == 'yes') {
                            debugger

                            urlResquest = urlResquest + "?id=" + Id + "&status=0";
                            $.get(urlResquest, function (result) {
                                if (result.success) {
                                    $this.closest('td').parent('tr').find(".chip").addClass("chip-danger").removeClass("chip-success").find(".chip-text").text("Ẩn");
                                    $this.find("i").addClass("icon-lock").removeClass("icon-unlock");
                                    $this.attr("title", "Mở khóa dữ liệu");
                                    toastr.success(result.message, 'Khóa dữ liệu');
                                }
                                else
                                    toastr.error(result.message, 'Khóa dữ liệu');
                            });
                        } else {
                            toastr.remove()
                        }
                    }
                });
        }

    });
}
var unlockDataTable = function (path) {
    $(document).on("click", ".action-unlock", function (e) {
        e.preventDefault();
        if (!isAction) {
            isAction = true;
            setTimeout(function () {
                isAction = false;
            }, 1000);
            //timeDelete = true;
            var $this = $(this)
            let urlResquest = path;
            debugger
            let Id = $this.attr("data-id");
            toastr.info("<br /><button type='button' class='btn btn-danger' value='yes'>Yes</button><button class='btn btn-default' type='button' value='no' >No</button>", 'Bạn có chắc chắn muốn mở khóa bản ghi ' + Id + ' ?',
                {
                    closeButton: false,
                    allowHtml: true,
                    onclick: function (toast) {
                        value = toast.target.value;

                        if (value == 'yes') {
                            debugger

                            urlResquest = urlResquest + "?id=" + Id + "&status=1";
                            $.get(urlResquest, function (result) {
                                if (result.success) {
                                    $this.closest('td').parent('tr').find(".chip").addClass("chip-success").removeClass("chip-danger").find(".chip-text").text("Hiển thị");
                                    $this.find("i").addClass("icon-unlock").removeClass("icon-lock");
                                    $this.attr("title", "Mở khỏa tài khoản");
                                    toastr.success(result.message, 'Khóa dữ liệu');
                                }
                                else
                                    toastr.error(result.message, 'Mở khóa dữ liệu');
                            });
                        } else {
                            toastr.remove()
                        }
                    }
                });
        }

    });
}
//action dùng cho id string
var deleteDataTable_S = function (path) {
    debugger
    $(document).on("click", ".action-delete", function (e) {

        e.preventDefault();
        if (!isAction) {
            isAction = true;
            setTimeout(function () {
                isAction = false;
            }, 1000);
            //timeDelete = true;
            var $this = $(this)
            let urlResquest = path;
            debugger
            let Id = $this.attr("data-id");
            toastr.info("<br /><button type='button' class='btn btn-danger' value='yes'>Yes</button><button class='btn btn-default' type='button' value='no' >No</button>", 'Bạn có chắc chắn muốn xóa bản ghi ' + Id + ' ?',
                {
                    closeButton: false,
                    allowHtml: true,
                    onclick: function (toast) {
                        value = toast.target.value;

                        if (value == 'yes') {
                            debugger

                            urlResquest = urlResquest + "?id=" + encodeURIComponent(Id);
                            $.get(urlResquest, function (result) {
                                if (result.success) {
                                    $this.closest('td').parent('tr').fadeOut();
                                    toastr.success(result.message, 'Xóa dữ liệu');
                                }
                                else
                                    toastr.error(result.message, 'Xóa dữ liệu');
                            });
                        } else {
                            toastr.remove()
                        }
                    }
                });



        }

    });
}
var editDataTable_S = function () {
    $(".action-edit").on("click", function (e) {
        e.preventDefault();

        $(this).closest("tr").find("[x-name]").each(function (index, item) {
            var type = $(item).attr("x-name");
            $("[name=" + type + "]").val($(item).attr("x-value"));


        });
        $(".add-new-data").addClass("show")
        $(".overlay-bg").addClass("show")
    })
}

function resizeEL() {
    debugger
    var h = $(window).width();
    if ($('.ui-dialog').width() != h) {
        $('.ui-dialog').width(h).resize();
    }

}


//action dùng cho id string

$(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function () {
    $(".add-new-data").removeClass("show")
    $(".overlay-bg").removeClass("show")
    $("#data-name, #data-price").val("")
    $("#data-category, #data-status").prop("selectedIndex", 0)
})

$.fn.loadField = function (lstObj) {
    $(this).each(function (index, item) {
        var name = $(item).attr("x-name");
        var xclass = $(item).attr("x-class");
        var xsort = $(item).attr("x-sort");

        if (name == "image") {

            lstObj.push(
                {
                    sortable: false,
                    class: "product-img " + xclass,
                    "render": function (data, type, full, meta) { return '<img src="' + full.image + '" alt="Image">'; }
                },

            );
        } else {
            xsort = (xsort != undefined && xsort != null) ? xsort : false;
            lstObj.push(
                { data: name, name: name, class: xclass, sortable: xsort },
            );
        }
    });
}

function initTinymce() {
    tinymce.init({
        selector: "textarea.tinymce", theme: "modern", height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| link unlink anchor | image media | forecolor backcolor  | print preview code ",
        image_advtab: true,
        file_picker_callback: elFinderBrowser
    });
}
function elFinderBrowser(callback, value, meta) {
    tinymce.activeEditor.windowManager.open({
        file: '/tiny-mce/browse',
        title: 'File Manager',
        width: 900,
        height: 450,
        resizable: 'yes'
    }, {
        oninsert: function (file, fm) {

            var url, reg, info;

            // URL normalization
            url = fm.convAbsUrl(file.url);

            // Make file info
            info = file.name + ' (' + fm.formatSize(file.size) + ')';

            // Provide file and text for the link dialog
            if (meta.filetype == 'file') {
                callback(url, { text: info, title: info });
            }

            // Provide image and alt text for the image dialog
            if (meta.filetype == 'image') {
                callback(url, { alt: info });
            }

            // Provide alternative source and posted for the media dialog
            if (meta.filetype == 'media') {
                callback(url);
            }
        }
    });
    return false;
}

$.fn.elFinderBrowser_Show = function (ele, callBack) {
    var myCommands = elFinder.prototype._options.commands;
    //resizeEL();
    var $this = $(ele);

    var elfinder = $(this).elfinder({
        url: '/el-finder/file-system/connector',
        resizable: false,
        onlyMimes: ["image"],
        commands: myCommands,
        uiOptions: {
            // toolbar configuration
            toolbar: [
                ["reload"],
                ["open", "download", "getfile"],
                ["duplicate", "rename", "edit", "resize"],
                ["quicklook", "info"],
                ["search"],
                ["view", "sort"]
            ]
        },
        getfile: {
            onlyURL: true,
            multiple: false,
            folders: false,
            oncomplete: ""
        },
        handlers: {
            dblclick: function (event, elfinderInstance) {
                fileInfo = elfinderInstance.file(event.data.file);

                if (fileInfo.mime != "directory") {

                    var imgURL = elfinderInstance.url(event.data.file);
                    var url = new URL(imgURL);
                    callBack($this, url.pathname);

                    //$("#page_image").val(imgURL);

                    //var imgPath = '<img src="' + imgURL + '" id="append-image" style="width:280px;height:auto;background-size:contain;margin-bottom:.9em;background-repeat:no-repeat"/>';
                    //$("#elfinder_image").append(imgPath); //add the image to a div so you can see the selected images

                    //$("#remove_image").show();
                    //$("#set_image").hide();

                    elfinderInstance.destroy();
                    return false; // stop elfinder
                };
            },
            destroy: function () {
                elfinder.dialog("close");

            }
        }
    }).dialog({
        title: "filemanager",
        resizable: false,
        width: 920,
        height: 500
    });
}

function submitForm(ele) {
    var info = $(ele).serialize();
    var action = $(ele).attr("action");

    $.ajax({
        url: action,
        method: $(ele).attr("method"),
        data: info,
        dateType: 'json',
        beforeSend: function () {
            common.startLoading();
        },
    }).done(function (res) {
        if (res.success) {
            toastr.success(res.message)
            common.stopLoading();
            try {
                var chk = $("#chk_main").val()
                if (chk) {
                    var indexOf = action.lastIndexOf("/");
                    location.href = action.substring(0, indexOf);
                } else {
                    if (res.url != null && res.url != undefined && res.url.length > 0) {
                        location.href = res.url;
                    } else {
                        location.reload();
                    }
                }
            } catch (e) {

            }
        } else {
            common.stopLoading();
            toastr.error(res.message)
        }
    });
}
function submitForm_Messs(ele) {
    var info = $(ele).serialize();
    var action = $(ele).attr("action");

    $.ajax({
        url: action,
        method: $(ele).attr("method"),
        data: info,
        dateType: 'json',
        beforeSend: function () {
            common.startLoading();
        },
    }).done(function (res) {
        if (res.success) {
            toastr.success(res.message)
            common.stopLoading();
            
        } else {
            common.stopLoading();
            toastr.error(res.message)
        }
    });
}
function submitForm_ExistImage(ele) {
    var redirect = $(ele).attr("redirect");
    var action = $(ele).attr("action");
    var formData = new FormData($(ele)[0]);
    $.ajax({
        url: action,
        method: $(ele).attr("method"),
        data: formData,
        dateType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            common.startLoading();
        },
    }).done(function (res) {
        if (res.success) {
            toastr.success(res.message)
            common.stopLoading();
            try {

                if (redirect != null && redirect != undefined && redirect.length > 0) {
                    location.href = redirect;
                } else {
                    var chk = $("#chk_main").val()
                    if (chk) {
                        var indexOf = action.lastIndexOf("/");
                        location.href = action.substring(0, indexOf);
                    } else {
                        if (res.url != null && res.url != undefined && res.url.length > 0) {
                            location.href = res.url;
                        } else {
                            location.reload();
                        }
                    }
                }

            } catch (e) {

            }
        } else {
            toastr.error(res.message)
            common.stopLoading();
        }
    });
}

function loadInfo() {
    var valu = $("[x-image=one]").attr("src");
    if (valu != "/img/upload.png") {
        $("input[name=Image]").val(valu.replace("/Files", ""));
    }
    var content = tinyMCE.editors[$('#Content').attr('id')].getContent();
    $("#Content").val(content)

    var des = tinyMCE.editors[$('#Description').attr('id')].getContent();
    $("#Description").val(des)
}

function genUrl(ele, str) {
    $(ele).val(clean_url($(str).val()));
}

function clean_url(s) {
    return s.toString().normalize('NFD').replace(/[\u0300-\u036f]/g, "")
        .toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/&/g, '-and-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

var config = {
    module: ""
}

$(document).on("input", "input[type=currency]", function (e) {
    $(this).val(formatCurrency(this.value.replace(/[,]/g, '')));
});

$(document).on('keypress', "input[type=currency]", function (e) {
    if (!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
});
$(document).on('paste', "input[type=currency]", function (e) {
    var cb = e.originalEvent.clipboardData || window.clipboardData;
    if (!$.isNumeric(cb.getData('text'))) e.preventDefault();
});
function formatCurrency(number) {
    var n = number.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
    return n2.split('').reverse().join('');
}
function formatCurrencyNumber(un) {
    if (un !== undefined && un !== null) {
        let number = un.toString();
        var n = number.split('').reverse().join("");
        var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
        n2 = n2.replace(",-", "-");
        return n2.split('').reverse().join('');
    }
    else {
        return "";
    }
}

$("input[type='currency']").each(function () {
    $(this).val(formatCurrencyNumber($(this).val()));
});

$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });


});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}


var common = {
    
    startLoading: function () {
        $(".overlay").fadeIn("fast", function () {
            $("body").addClass("loading");
        });
    },
    stopLoading: function () {
        $(".overlay").fadeOut("fast", function () {
            $("body").removeClass("loading");
        });
    }
};