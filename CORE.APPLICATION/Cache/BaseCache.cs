﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public interface IBaseCache
    {
        Dictionary<int, Product> GetByCategory(string appId);
        Dictionary<int, Product> GetByBrand();
        Dictionary<string, Function> GetByFunction();
        Dictionary<string, Color> GetByColor();
        Dictionary<string, AppRole> GetByAppRole();
        Dictionary<string, string> GetByEnterprises();
    }

    public class BaseCache : IBaseCache
    {
        IFunctionService _functionService;
        IProductService _productService;
        IMemoryCache _memoryCache;
        IColorService _colorService;
        IAppRoleService _appRoleService;
        IEnterprisesService _enterprisesService;
        public BaseCache(IProductService productService, IMemoryCache memoryCache, IFunctionService functionService, IColorService colorService, IAppRoleService appRoleService, IEnterprisesService enterprisesService)
        {
            _memoryCache = memoryCache;
            _productService = productService;
            _functionService = functionService;
            _colorService = colorService;
            _appRoleService = appRoleService;
            _enterprisesService = enterprisesService;
        }
        public Dictionary<int, Product> GetByBrand()
        {
            string key = "Admin";
            string firstK = "BR";
            //if (_memoryCache.TryGetValue(firstK + key, out Dictionary<int, Product> blahs))
            //{
            //    return blahs;
            //}
            var data = _productService.FindAll(x => x.Type == DATA.Enums.TypeContent.News && x.TypePost == DATA.Enums.TypeNews.Brand, new string[] { "Id", "Name", "Type", "TypePost", "Status" }).ToDictionary(x => x.Id, x => x);
            //_memoryCache.Set(firstK + key, data, Setup.Util.OptionsCache(10, 2));
            return data;
        }

        public Dictionary<int, Product> GetByCategory(string appId)
        {
            string key = "Admin";
            string firstK = "CAT";
            //if (_memoryCache.TryGetValue(firstK + key, out Dictionary<int, Product> blahs))
            //{
            //    return blahs;
            //}
            var data = _productService.FindAll(x => x.AppId == appId && x.Type == DATA.Enums.TypeContent.Zone, new string[] { "Id", "Name", "Type", "TypePost", "Status", "ParentId" }).ToDictionary(x => x.Id, x => x);
            //_memoryCache.Set(firstK + key, data, Setup.Util.OptionsCache(10, 2));
            return data;
        }

        public Dictionary<string, Color> GetByColor()
        {
            string key = "Admin";
            string firstK = "Cl";
            if (_memoryCache.TryGetValue(firstK + key, out Dictionary<string, Color> blahs))
            {
                return blahs;
            }
            var data = _colorService.FindAll(x => x.Status == DATA.Enums.Status.Active).ToDictionary(x => x.Id, x => x);
            _memoryCache.Set(firstK + key, data, Setup.Util.OptionsCache(60, 2));
            return data;
        }

        public Dictionary<string, Function> GetByFunction()
        {
            string key = "Admin";
            string firstK = "Fc";
            if (_memoryCache.TryGetValue(firstK + key, out Dictionary<string, Function> blahs))
            {
                return blahs;
            }
            var data = _functionService.FindAll(x => x.Status == DATA.Enums.Status.Active).ToDictionary(x => x.Base, x => x);
            _memoryCache.Set(firstK + key, data, Setup.Util.OptionsCache(60, 2));
            return data;
        }

        public Dictionary<string, AppRole> GetByAppRole()
        {
            string key = "Admin";
            string firstK = "R";
            //if (_memoryCache.TryGetValue(firstK + key, out Dictionary<string, AppRole> blahs))
            //{
            //    return blahs;
            //}
            var data = _appRoleService.FindAll().ToDictionary(x => x.Id.ToString(), x => x);
            _memoryCache.Set(firstK + key, data, Setup.Util.OptionsCache(60, 2));
            return data;
        }

        public Dictionary<string, string> GetByEnterprises()
        {
            string key = "Admin";
            string firstK = "Eter";
            //if (_memoryCache.TryGetValue(firstK + key, out Dictionary<string, string> blahs))
            //{
            //    return blahs;
            //}
            var data = _enterprisesService.FindAll().ToDictionary(x => x.Id.ToString(), x => $"{x.Id}-{x.Name}");
            _memoryCache.Set(firstK + key, data, Setup.Util.OptionsCache(60, 2));
            return data;
        }
    }
}


