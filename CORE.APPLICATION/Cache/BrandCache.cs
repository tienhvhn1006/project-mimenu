﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public static partial class ZoneCache
    {
        #region Thuong Hieu

        public static List<Product> AllBrandByType(this IBaseCache product)
        {
            var All = product.GetByBrand().Values.ToList();
            return All.ToList();
        }
        #endregion
    }
}
