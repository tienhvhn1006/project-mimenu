﻿using CORE.DATA.Entities;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public static class ColorCache
    {
        #region Category
        public static List<Color> AllColor(this IBaseCache product)
        {
            var All = product.GetByColor().Values;
            return All.ToList();
        }
        #endregion
    }
}
