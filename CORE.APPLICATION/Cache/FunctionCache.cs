﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public static partial class ZoneCache
    {
        #region Function
       
        public static List<TreeModelMenu> Menus(this IBaseCache function, List<string> lstRole)
        {
            var data = function.GetByFunction();
            var trees = Setup.Util.GetTreeMenu(data.Values.ToList(), null);
            return trees;
        }
        #endregion
    }
}
