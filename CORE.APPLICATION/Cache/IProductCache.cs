﻿using CORE.APPLICATION.Cache.Object;
using CORE.APPLICATION.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{



    public interface IProductCache
    {
        public List<Object.OrderProduct> ProductOrder(string appId);
    }
    public class ProductCache : IProductCache
    {
        IProductService _productService;
        ITypeProductService _typeProductService;
        IMemoryCache _memoryCache;
        public ProductCache(IProductService productService, ITypeProductService typeProductService, IMemoryCache memoryCache)
        {
            _productService = productService;
            _typeProductService = typeProductService;
            _memoryCache = memoryCache;
        }

        public List<OrderProduct> ProductOrder(string appId)
        {
            string key = "OrderP"+ appId;
            if (_memoryCache.TryGetValue(key, out List<OrderProduct> blahs))
            {
                return blahs;
            }
            var data = _productService.FindAll(x => x.AppId == appId && x.IsDeleted == false && x.Status == DATA.Enums.Status.Active).Join(_typeProductService.FindAll(), x => x.Id, x => x.Id,
                 (pro, typePro) => new { Pro = pro, TypePro = typePro })
                .Select(x => new { x.Pro.Id, Price= x.TypePro.DiscountPrice, x.Pro.Name }).ToList()
                .Select(x => new OrderProduct() { Id = x.Id, Name = x.Name, Price = x.Price }).ToList();

            _memoryCache.Set(key, data, Setup.Util.OptionsCache(60, 60));
            return data;
        }
    }
}
