﻿using CORE.APPLICATION.Cache.Object;
using CORE.APPLICATION.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public interface IPromotionCache
    {
        public List<Object.Promotion> Promotion(string appId);

    }
    public class PromotionCache : IPromotionCache
    {
        IPromotionService _promotionService;
        IMemoryCache _memoryCache;
        public PromotionCache(IPromotionService promotionService, IMemoryCache memoryCache)
        {
            _promotionService = promotionService;
            _memoryCache = memoryCache;
        }

        public List<Promotion> Promotion(string appId)
        {
            string key = "PromotionAll" + appId;
            if (_memoryCache.TryGetValue(key, out List<Promotion> blahs))
            {
                return blahs;
            }
            var data = _promotionService.FindAll(x => x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now
                  && x.AppId == appId
                  && x.Status == DATA.Enums.Status.Active, new string[] { "Id", "Name","Code", "Discounts", "MaxValue", "Type" }).ToList()
                .Select(x => new Object.Promotion { Id = x.Id, Name = x.Name, Code = x.Code, Discounts = x.Discounts, MaxValue = x.MaxValue, Type = (int)x.Type }).ToList();
            _memoryCache.Set(key, data, Setup.Util.OptionsCache(60, 60));
            return data;
        }
    }
}
