﻿using CORE.APPLICATION.Cache.Object;
using CORE.APPLICATION.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public interface IQRCodeCache
    {
        public List<Object.QRCode> QROrder(string appId);
    }
    public class QRCodeCache : IQRCodeCache
    {
        IQRCodeService _qRCodeService;
        IMemoryCache _memoryCache;
        public QRCodeCache(IQRCodeService qRCodeService, IMemoryCache memoryCache)
        {
            _qRCodeService = qRCodeService;
            _memoryCache = memoryCache;
        }
        public List<QRCode> QROrder(string appId)
        {
            string key = "QRCodeAll" + appId;
            if (_memoryCache.TryGetValue(key, out List<QRCode> blahs))
            {
                return blahs;
            }
            var data = _qRCodeService.FindAll(x => x.IsDeleted == false
                  && x.AppId == appId
                  && x.Status == DATA.Enums.Status.Active, new string[] { "Id", "Name" }).ToList()
                .Select(x => new Object.QRCode { Id = x.Id, Name = x.Name }).ToList();
            _memoryCache.Set(key, data, Setup.Util.OptionsCache(60, 60));
            return data;
        }
    }
}
