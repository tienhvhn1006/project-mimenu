﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Cache.Object
{
    public class OrderProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
