﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Cache.Object
{
    public class Promotion
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Type { get; set; }
        public double Discounts { get; set; }
        public double MaxValue { get; set; }

        public string GetName()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Mã " + this.Code + " -");
            if (Type == (int)DATA.Enums.TypePromotion.Percent)
            {
                sb.Append($" Giảm {this.Discounts} % ");
            }
            else if (Type == (int)DATA.Enums.TypePromotion.FreeShip)
            {
                sb.Append(" Miễn phí vận chuyển ");
            }
            else
            {
                sb.Append($" Giảm {CORE.UTILITY.MUtility.FomatMoney(this.Discounts)}");
            }
            return sb.ToString();
        }
    }


}
