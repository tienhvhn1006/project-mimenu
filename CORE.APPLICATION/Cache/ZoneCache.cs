﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Cache
{
    public static partial class  ZoneCache
    {
        #region Category
        public static List<Product> AllCateroryByType(this IBaseCache product, DATA.Enums.TypeNews typeP,string appId)
        {
            var All = product.GetByCategory(appId).Values.Where(x => x.TypePost == typeP);
            return All.ToList();
        }
        #endregion
    }
}
