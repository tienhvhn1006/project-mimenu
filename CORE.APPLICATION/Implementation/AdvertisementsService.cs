﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class AdvertisementsService : Repository<Advertisement>, IAdvertisementsService, IDisposable
    {
        public AdvertisementsService(AppDbContext context) : base(context)
        {

        }

        public bool AddAds(Advertisement obj)
        {
            bool result = false;
            if (obj.Id > 0)
            {
                result= this.Update(obj, true);
            }
            else
            {
                result = this.Add(obj, true);
            }
            return result;
        }
    }
}
