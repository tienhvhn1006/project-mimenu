﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class AppRoleService : Repository<AppRole>, IAppRoleService, IDisposable
    {
        IAppRoleUserService _appRoleUserService;
        public AppRoleService(AppDbContext context, IAppRoleUserService appRoleUserService) : base(context)
        {
            _appRoleUserService = appRoleUserService;
        }

        public bool DeleteRole(Guid id)
        {
            bool result = false;
            try
            {
                _appRoleUserService.RemoveMultiple(_appRoleUserService.FindAll(x => x.RoleId == id).ToList());
                this.Remove(new AppRole() { Id = id });
                this.Commit();
                result = true;
            }
            catch (Exception ex)
            {
            }
          
            return result;
        }
    }
}
