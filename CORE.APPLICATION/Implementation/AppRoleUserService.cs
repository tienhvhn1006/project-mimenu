﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class AppRoleUserService : Repository<IdentityUserRole<Guid>>, IAppRoleUserService, IDisposable
    {
        public AppRoleUserService(AppDbContext context) : base(context)
        {

        }

        public bool AddMutil(List<Guid> lstRole, Guid userId)
        {
            var result = false;
            try
            {
                foreach (var item in lstRole)
                {
                    this.Add(new IdentityUserRole<Guid>() { RoleId = item, UserId = userId });
                }
                this.Commit();
                result = true;
            }
            catch 
            {}
            return result;
        }
    }
}
