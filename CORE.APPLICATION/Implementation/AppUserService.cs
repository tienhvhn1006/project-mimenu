﻿using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.APPLICATION.Implementation
{
    public class AppUserService : Repository<AppUser>, IAppUserService, IDisposable
    {

        IAppRoleUserService _appRoleUserService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;

        private readonly IEmailService _emailService;
     
        public AppUserService(AppDbContext context, IAppRoleUserService appRoleUserService,
            IEmailService emailService,
            IConfiguration configuration,
            UserManager<AppUser> userManager
            ) : base(context)
        {
            _appRoleUserService = appRoleUserService;
            _emailService = emailService;
            _configuration = configuration;
            _userManager = userManager;
        }


        public List<LAppUserVM> LoadData(FilterString filter, out int total)
        {
            total = 0;

            var result = this.FindAll();

            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.FullName.Contains(filter.sSearch) || x.Id.Equals(filter.sSearch));
            }
            if (String.IsNullOrEmpty(filter.Sort))
            {
                filter.Sort = "DateCreated";
            }
            result = filter.SortDir == DATA.Enums.SortDir.ASC ? result.OrderBy(filter.Sort) : result.OrderByDescending(filter.Sort);

            total = result.Count();
            var data = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength).Select(x => new { x.Id, x.Email, x.AppId, x.FullName, x.UserName, x.DateCreated, x.DateModified, x.Status }).ToList();
            return data.Select(x => new LAppUserVM
            {
                Id = x.Id.ToString(),
                //AppId = x.AppId,
                FullName = x.FullName,
                UserName = x.UserName,
                Email = x.Email,
                Status = UiHelper.ShowHtmlStatus(x.Status),
                Date = $"<p>Ngày tạo: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày sửa: {x.DateModified.ToString("dd/MM/yyyy HH:mm")}</p>",
                Action = UiHelper.ShowHtmlActionAccount(x.UserName, x.Status),
            }).ToList();
        }

        public bool UnLock(AppUser appUser)
        {
            bool result = false;
            try
            {
                this._context.AppUsers.Attach(appUser);
                var entry = this._context.Entry(appUser);
                entry.Property(x => x.Status).IsModified = true;
                entry.Property(x => x.DateModified).IsModified = true;
                this.Commit();
                result = true;
            }
            catch (Exception ex)
            { }
            return result;
        }
        public async Task GenerateForgotPasswordTokenAsync(AppUser user)
        {
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            if (!string.IsNullOrEmpty(token))
            {
                await SendForgotPasswordEmail(user, token);
            }
        }
        private async Task SendForgotPasswordEmail(AppUser user, string token)
        {
            string appDomain = _configuration.GetSection("Application:AppDomain").Value;
            string confirmationLink = _configuration.GetSection("Application:ForgotPassword").Value;

            UserEmailOptions options = new UserEmailOptions
            {
                ToEmails = new List<string>() { user.Email },
                PlaceHolders = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("{{UserName}}", user.UserName),
                    new KeyValuePair<string, string>("{{Link}}",
                        string.Format(appDomain + confirmationLink, user.Id, token))
                }
            };

            await _emailService.SendEmailForForgotPassword(options);
        }
        public bool UpdateInfo(AppUser appUser, List<Guid> lstRole)
        {
            var result = false;
            try
            {
                this._context.AppUsers.Attach(appUser);
                var entry = this._context.Entry(appUser);
                entry.Property(x => x.AppId).IsModified = true;
                entry.Property(x => x.FullName).IsModified = true;
                entry.Property(x => x.Avatar).IsModified = true;
                entry.Property(x => x.BirthDay).IsModified = true;
                entry.Property(x => x.PhoneNumber).IsModified = true;
                entry.Property(x => x.DateModified).IsModified = true;


                _appRoleUserService.RemoveMultiple(_appRoleUserService.FindAll(x => x.UserId == appUser.Id).ToList());

                foreach (var item in lstRole)
                {
                    _appRoleUserService.Add(new Microsoft.AspNetCore.Identity.IdentityUserRole<Guid>() { RoleId = item, UserId = appUser.Id });
                }
                this.Commit();
                result = true;
            }
            catch (Exception ex)
            {


            }
            return result;
        }

        public async Task<IdentityResult> ResetPasswordAsync(ResetPasswordVM model)
        {
            return await _userManager.ResetPasswordAsync(await _userManager.FindByIdAsync(model.UserId), model.Token, model.NewPassword);
        }
    }
}
