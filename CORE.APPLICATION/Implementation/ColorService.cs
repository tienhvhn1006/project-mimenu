﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class ColorService : Repository<Color>, IColorService, IDisposable
    {
        public ColorService(AppDbContext context) : base(context)
        {

        }

        public bool AddColor(Color obj)
        {
            bool result = false;
            try
            {
                if(this._context.Colors.Any(x=>x.Id == obj.Id))
                {
                    result=  this.Update(obj, true);
                }
                else
                {
                    result= this.Add(obj, true);
                }
                
            }
            catch (Exception ex)
            {

            }
            return result;
        }
      

    }
}
