﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class ConfigsService : Repository<Config>, IConfigsService, IDisposable
    {
        public ConfigsService(AppDbContext context) : base(context)
        {

        }
    }
}
