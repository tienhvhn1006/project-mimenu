﻿using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class CustomerService : Repository<Customer>, ICustomerService, IDisposable
    {
        public CustomerService(AppDbContext context) : base(context)
        {

        }
        public List<CustomerVM> LoadData(FilterStringCustomer filter, out int total)
        {
            var result = this.FindAll(x => x.AppId == filter.AppId);

            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.Phone.Contains(filter.sSearch) || x.Name.Contains(filter.sSearch));
            }
            if (filter.Type != DATA.Enums.TypeCustomer.All)
            {
                result = result.Where(x => x.TypeCustomer == filter.Type);
            }
            string sort = "";

            switch (filter.iSortCol_0)
            {
                case 5:
                    sort = "Birthday";
                    break;
                default:
                    sort = "Id";
                    break;
            }

            result = filter.sSortDir_0 == "desc" ? result.OrderBy(sort) : result.OrderByDescending(sort);
            total = result.Count();
            var response = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength).ToList();

            return response.Select(x => new CustomerVM()
            {
                Id = x.Id,
                Name = x.Name,
                Birthday = x.Birthday.ToString("dd/MM/yyyy HH:mm"),
                Phone = x.Phone,
                Type = UiHelper.ShowHtmlTypeCustomer(x.TypeCustomer),
                Action = UiHelper.ShowHtmlActionEdit(x.Id)
            }).ToList();
        }
        public bool AddOrUpdate(Customer obj)
        {
            var isSuccess = false;
            if (obj.Id > 0)
            {
                isSuccess = this.Update(obj, true);
            }
            else
            {
                isSuccess = this.Add(obj, true);
            }
            return isSuccess;
        }
    }
}
