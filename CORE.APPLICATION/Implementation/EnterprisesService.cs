﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class EnterprisesService : Repository<Enterprise>, IEnterprisesService, IDisposable
    {
        public EnterprisesService(AppDbContext context) : base(context)
        {

        }

        public bool AddEnterprise(Enterprise obj)
        {
            bool result = false;
            try
            {
                if (this._context.Enterprises.Any(x => x.Id == obj.Id))
                {

                    result = this.Update(obj, true);
                }
                else
                {
                    obj.CreatedBy = obj.ModifiedBy;
                    obj.DateCreated = obj.DateModified;
                    result = this.Add(obj, true);
                }
              
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
