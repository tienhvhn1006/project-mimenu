﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class FunctionService : Repository<Function>, IFunctionService, IDisposable
    {
        public FunctionService(AppDbContext context) : base(context)
        {

        }
    }
}
