﻿using CORE.APPLICATION.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class HelperUpload : IHelperUpload
    {
        IHostingEnvironment _env;
        public HelperUpload(IHostingEnvironment env)
        {
            _env = env;
        }
        public string UploadImage(string PathRoot, string AppId, List<IFormFile> files)
        {
            var webRoot = _env.WebRootPath;
            string fileName = "";
            try
            {
                var pathToSave = Path.Combine(webRoot, PathRoot);
                foreach (var file in files)
                {
                    var ext = Path.GetExtension(PathRoot + "\\" + file.FileName);

                    fileName = Path.GetFileName(PathRoot + "\\" + AppId + "_" + DateTime.Now.ToString("dd-MM-yyyyHHmmss") + "_" + CORE.UTILITY.MUtility.UnicodeToKoDauAndGach(file.Name) + ext);

                    var fullPath = Path.Combine(pathToSave, fileName);
                    if (!Directory.Exists(pathToSave))
                    {
                        Directory.CreateDirectory(pathToSave);
                    }
                    #region Check Size
                    //if ((file.Length / 1024) < 1) //Lớn hơn 10MB
                    //{
                    //    using (var stream = new FileStream(fullPath, FileMode.Create))
                    //    {
                    //        file.CopyTo(stream);

                    //        var thumbPathToSave = Path.Combine(webRoot, pathToSave, ".tmb");
                    //        if (!Directory.Exists(thumbPathToSave))
                    //        {
                    //            Directory.CreateDirectory(thumbPathToSave);
                    //        }
                    //        var thumbPath = Path.Combine(thumbPathToSave, fileName);
                    //        using (var stream1 = file.OpenReadStream())
                    //        {
                    //            var uploadedImage = Image.FromStream(stream1);

                    //            Bitmap origBMP = new Bitmap(uploadedImage);
                    //            if (origBMP.Width <= 150)
                    //            {
                    //                origBMP.Save(thumbPath);
                    //            }
                    //            else
                    //            {
                    //                int origWidth = origBMP.Width;
                    //                int origHeight = origBMP.Height;
                    //                int newWidth = 150;
                    //                int newHeight = newWidth * origHeight / origWidth;

                    //                Bitmap newBMP = new Bitmap(origBMP, newWidth, newHeight);
                    //                Graphics objGra = Graphics.FromImage(newBMP);
                    //                objGra.DrawImage(origBMP, new Rectangle(0, 0, newBMP.Width, newBMP.Height), 0, 0, origWidth, origHeight, GraphicsUnit.Pixel);
                    //                objGra.Dispose();
                    //                newBMP.Save(thumbPath);
                    //            }
                    //            //file.CopyTo(stream1);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    #endregion
                    using (var stream = file.OpenReadStream())
                    {
                        var uploadedImage = Image.FromStream(stream);

                        Bitmap origBMP = new Bitmap(uploadedImage);
                        if (origBMP.Width <= 300)
                        {
                            origBMP.Save(fullPath);
                        }
                        else
                        {
                            int origWidth = origBMP.Width;
                            int origHeight = origBMP.Height;
                            int newWidth = 300;
                            int newHeight = newWidth * origHeight / origWidth;

                            Bitmap newBMP = new Bitmap(origBMP, newWidth, newHeight);
                            Graphics objGra = Graphics.FromImage(newBMP);
                            objGra.DrawImage(origBMP, new Rectangle(0, 0, newBMP.Width, newBMP.Height), 0, 0, origWidth, origHeight, GraphicsUnit.Pixel);
                            objGra.Dispose();
                            newBMP.Save(fullPath);
                        }

                        var thumbPathToSave = Path.Combine(webRoot, pathToSave, ".tmb");
                        if (!Directory.Exists(thumbPathToSave))
                        {
                            Directory.CreateDirectory(thumbPathToSave);
                        }
                        var thumbPath = Path.Combine(thumbPathToSave, fileName);
                        using (var stream1 = file.OpenReadStream())
                        {
                            uploadedImage = Image.FromStream(stream1);

                            origBMP = new Bitmap(uploadedImage);
                            if (origBMP.Width <= 150)
                            {
                                origBMP.Save(thumbPath);
                            }
                            else
                            {
                                int origWidth = origBMP.Width;
                                int origHeight = origBMP.Height;
                                int newWidth = 150;
                                int newHeight = newWidth * origHeight / origWidth;

                                Bitmap newBMP = new Bitmap(origBMP, newWidth, newHeight);
                                Graphics objGra = Graphics.FromImage(newBMP);
                                objGra.DrawImage(origBMP, new Rectangle(0, 0, newBMP.Width, newBMP.Height), 0, 0, origWidth, origHeight, GraphicsUnit.Pixel);
                                objGra.Dispose();
                                newBMP.Save(thumbPath);
                            }
                            //file.CopyTo(stream1);
                        }
                    }
                    //}

                }
            }
            catch (Exception ex)
            {

            }
            return "/" + fileName;
        }
    }
}
