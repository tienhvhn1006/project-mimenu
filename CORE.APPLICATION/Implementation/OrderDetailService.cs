﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class OrderDetailService : Repository<OrderDetail>, IOrderDetailService, IDisposable
    {
        public OrderDetailService(AppDbContext context) : base(context)
        {

        }
    }
}
