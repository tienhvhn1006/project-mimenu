﻿using CORE.DATA.EF;
using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public class OrderPromotionService : Repository<OrderPromotion>, IOrderPromotionService, IDisposable
    {
        public OrderPromotionService(AppDbContext context) : base(context)
        {

        }
    }
}
