﻿using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class OrderService : Repository<Order>, IOrderService, IDisposable
    {
        IProductService _iProductService;
        public OrderService(AppDbContext context, IProductService iProductService) : base(context)
        {
            _iProductService = iProductService;
        }
        public Order GetById(int orderId)
        {
            var data = this.FindAll(x => x.Id == orderId).Include(x => x.OrderDetails).ToList();
            if (data != null && data.Any())
            {
                return data.FirstOrDefault();
            }
            else
            {
                return new Order();
            }
        }
        public List<LOrderVM> LoadData(FilterStringOrder filter, out int total)
        {
            var result = this.FindAll(x => x.IsDeleted == false && x.AppId == filter.AppId);

            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.Name.Contains(filter.sSearch) || x.Code.Contains(filter.sSearch) || x.Id.Equals(filter.sSearch));
            }
            string sort = "";

            switch (filter.iSortCol_0)
            {
                case 3:
                    sort = "LogPrice";
                    break;
                default:
                    sort = "CreatedDate";
                    break;
            }

            if (filter.Status != StatusOrder.All)
            {
                result = result.Where(x => x.Status == filter.Status);
            }



            result = filter.sSortDir_0 == "desc" ? result.OrderBy(sort) : result.OrderByDescending(sort);
            total = result.Count();
            result = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength);
            var response = result.Select(x => new
            {
                x.Id,
                x.Code,
                x.Name,
                x.Address,
                x.Email,
                x.Phone,
                x.Status,
                x.CreatedDate,
                x.LogPrice
            }).ToList()
                .Select(x => new LOrderVM()
                {
                    Id = x.Id,
                    Code = $"<span  data-id=\"{x.Id}\" class=\"action-view text-info\">{x.Code}</span>",
                    InfoUser = $"{x.Phone}",
                    Info = CORE.UTILITY.MUtility.FomatMoney(x.LogPrice),
                    Time = x.CreatedDate.ToString("dd/MM/yyyy HH:mm"),
                    Status = UiHelper.ShowHtmlStatusOrder(x.Status, x.Id),
                    Action = UiHelper.ShowHtmlActionOrder(x.Id)
                }).ToList();
            return response.ToList();
        }
        public Dictionary<string, int> StatisticalByMonth(DateTime dateFrom, string appId)
        {
            var trendData =
             (from d in this.FindAll(x => x.IsDeleted == false && x.AppId == appId &&  x.CreatedDate.Year == dateFrom.Year && x.CreatedDate.Month >= dateFrom.Month)
              group d by new
              {
                  Day = d.CreatedDate.Date,

              } into g
              select new
              {
                  Day = g.Key.Day,

                  Total = g.Count(),
              }
            ).AsEnumerable()
             .Select(g => new
             {
                 Period = g.Day,
                 Total = g.Total,
             }).OrderBy(x => x.Period);
            var result = trendData.ToDictionary(x => x.Period, x => x.Total);
            return CORE.UTILITY.MUtility.AddMissingMonth(result, dateFrom.Date);
        }
        public Dictionary<string, int> StatisticalByWeek(DateTime dateFrom, string appId)
        {
            var trendData =
             (from d in this.FindAll(x => x.IsDeleted == false && x.AppId == appId && x.CreatedDate >= dateFrom.Date.AddDays(-6))
              group d by new
              {
                  Day = d.CreatedDate.Date,

              } into g
              select new
              {
                  Day = g.Key.Day,

                  Total = g.Count(),
              }
            ).AsEnumerable()
             .Select(g => new
             {
                 Period = g.Day,
                 Total = g.Total,
             }).OrderBy(x => x.Period);
            var result = trendData.ToDictionary(x => x.Period, x => x.Total);
            return CORE.UTILITY.MUtility.AddMissingWeek(result, dateFrom.Date);
        }
        public Dictionary<string, int> StatisticalByYear(DateTime dateFrom, string appId)
        {
            var trendData =
             (from d in this.FindAll(x => x.IsDeleted == false && x.AppId == appId && x.CreatedDate.Year == dateFrom.Year)
              group d by new
              {
                  Year = d.CreatedDate.Year,
                  Month = d.CreatedDate.Month,
              } into g
              select new
              {
                  Year = g.Key.Year,
                  Month = g.Key.Month,
                  Total = g.Count(),
              }
            ).AsEnumerable()
             .Select(g => new
             {
                 Period = g.Month,
                 Total = g.Total,
             });
            var result = trendData.ToDictionary(x => x.Period, x => x.Total);
            return CORE.UTILITY.MUtility.AddMissingYear(result);
        }
        public Dictionary<string, double> StatisticalRevenueByMonth(DateTime dateFrom, string appId)
        {
            var trendData =
             (from d in this.FindAll(x => x.IsDeleted == false && x.AppId == appId && x.Status == StatusOrder.Success && x.CreatedDate.Year == dateFrom.Year && x.CreatedDate.Month >= dateFrom.Month)
              group d by new
              {
                  Day = d.CreatedDate.Date,

              } into g
              select new
              {
                  Day = g.Key.Day,

                  Total = g.Sum(x => x.LogPrice),
              }
            ).AsEnumerable()
             .Select(g => new
             {
                 Period = g.Day,
                 Total = g.Total,
             }).OrderBy(x => x.Period);
            var result = trendData.ToDictionary(x => x.Period, x => x.Total);
            return CORE.UTILITY.MUtility.AddMissingMonth(result, dateFrom.Date);
        }
        public Dictionary<string, double> StatisticalRevenueByWeek(DateTime dateFrom, string appId)
        {
            var trendData =
             (from d in this.FindAll(x => x.IsDeleted == false && x.AppId == appId && x.Status == StatusOrder.Success && x.CreatedDate >= dateFrom.Date.AddDays(-6))
              group d by new
              {
                  Day = d.CreatedDate.Date,
              } into g
              select new
              {
                  Day = g.Key.Day,
                  Total = g.Sum(x => x.LogPrice),
              }
            ).AsEnumerable()
             .Select(g => new
             {
                 Period = g.Day,
                 Total = g.Total,
             }).OrderBy(x => x.Period);
            var result = trendData.ToDictionary(x => x.Period, x => x.Total);
            return CORE.UTILITY.MUtility.AddMissingWeek(result, dateFrom.Date);
        }
        public Dictionary<string, double> StatisticalRevenueByYear(DateTime dateFrom, string appId)
        {
            var trendData =
            (from d in this.FindAll(x => x.IsDeleted == false && x.AppId == appId && x.Status == StatusOrder.Success && x.CreatedDate.Year >= dateFrom.Year)
             group d by new
             {
                 Year = d.CreatedDate.Year,
                 Month = d.CreatedDate.Month,
             } into g
             select new
             {
                 Year = g.Key.Year,
                 Month = g.Key.Month,
                 Total = g.Sum(x => x.LogPrice),
             }
           ).AsEnumerable()
            .Select(g => new
            {
                Period = g.Month,
                Total = g.Total,
            });
            var result = trendData.ToDictionary(x => x.Period, x => x.Total);
            return CORE.UTILITY.MUtility.AddMissingYear(result);
        }
        public Dictionary<int, int> StatisticsByStatus(DateTime dateFrom, DateTime dateTo, string appId)
        {
            var result = this.FindAll(x => x.IsDeleted == false && x.AppId == appId && x.CreatedDate >= dateFrom && x.CreatedDate <= dateTo).AsEnumerable().GroupBy(x => x.Status).ToDictionary(x => (int)x.Key, x => x.Count());
            return result;
        }
        public List<Order> Top5Order(string appId)
        {
            return this.FindAll(x => x.IsDeleted == false && x.AppId == appId, new string[] { "Status", "LogPrice", "Code", "Phone", "CreatedDate" }).OrderByDescending(x => x.CreatedDate).Take(5).ToList();
        }
        public List<KeyValuePair<string, string>> Top5Sale(DateTime dateFrom, DateTime dateTo, string appId)
        {
            var result = this.FindAll(x => x.IsDeleted == false && x.CreatedDate >= dateFrom && x.CreatedDate <= dateTo && x.AppId == appId).Join(
                  this._context.OrderDetails,
                  x => x.Id, y => y.OrderId,
                  (x, y) => new { x, y }).AsEnumerable().GroupBy(x => x.y.ProductId).Select(x => new { Key = x.Key, Count = x.Count() }).OrderByDescending(x => x.Count).Take(5).ToDictionary(x => x.Key, x => x.Count);
            var nameP = _iProductService.GetNameById(result.Keys.ToList());

            return result.Select(x => new KeyValuePair<string, string>(nameP.ContainsKey(x.Key) ? nameP[x.Key] : $"Id: {x.Key} bị xóa", x.Value.ToString())).ToList();
        }
        public List<KeyValuePair<string, string>> Top5Revenue(DateTime dateFrom, DateTime dateTo, string appId)
        {
            var result = this.FindAll(x => x.IsDeleted == false && x.CreatedDate >= dateFrom && x.CreatedDate <= dateTo && x.AppId == appId).Join(
                  this._context.OrderDetails,
                  x => x.Id, y => y.OrderId,
                  (x, y) => new { x, y }).AsEnumerable().GroupBy(x => x.y.ProductId).Select(x => new { Key = x.Key, Count = x.Sum(x => x.y.LogPrice) }).OrderByDescending(x => x.Count).Take(5).ToDictionary(x => x.Key, x => x.Count);
            var nameP = _iProductService.GetNameById(result.Keys.ToList());

            return result.Select(x => new KeyValuePair<string, string>(nameP.ContainsKey(x.Key) ? nameP[x.Key] : $"Id: {x.Key} bị xóa", UTILITY.MUtility.FomatMoney(x.Value))).ToList();
        }
        public bool UpdateDeleted(int orderId)
        {
            bool result = false;
            try
            {
                var order = new Order() { Id = orderId, IsDeleted = true, };
                this._context.Orders.Attach(order);
                var entry = this._context.Entry(order);
                entry.Property(x => x.IsDeleted).IsModified = true;
                //entry.Property(x => x.DateModified).IsModified = true;
                this.Commit();
                result = true;
            }
            catch (Exception ex)
            { }
            return result;

        }
        public bool UpdateStatus(Order order)
        {
            bool result = false;
            try
            {
                this._context.Orders.Attach(order);
                var entry = this._context.Entry(order);
                entry.Property(x => x.Status).IsModified = true;
                //entry.Property(x => x.DateModified).IsModified = true;
                this.Commit();
                result = true;
            }
            catch (Exception ex)
            { }
            return result;
        }
    }
}
