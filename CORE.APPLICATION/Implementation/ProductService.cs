﻿using CORE.APPLICATION.Cache;
using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class ProductService : Repository<Product>, IProductService, IDisposable
    {

        public ProductService(AppDbContext context) : base(context)
        {

        }
        #region Delete
        public bool Delete(int id)
        {
            try
            {

                this._context.Products.Remove(new Product { Id = id });
                if (this._context.TypeProducts.Any(x => x.Id == id))
                {
                    this._context.TypeProducts.Remove(new TypeProduct { Id = id });
                }

                this.Commit();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public bool DeleteZone(int id)
        {
            if (!this._context.Products.Any(x => x.ParentId == id))
            {
                this.Remove(new Product() { Id = id });
                this.Commit();
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool DeleteArticle(int id)
        {
            this.Remove(new Product() { Id = id });
            this.Commit();
            return true;
        }
        #endregion
        #region Load
        public List<LProductVM> LoadData(Filter filter, CORE.DATA.Enums.TypeContent type, out int total)
        {
            total = 0;

            var result = this.FindAll(x => x.Type == type && x.AppId == filter.AppId).GroupJoin(this._context.TypeProducts, a => a.Id, b => b.Id,
                        (pd, tpd) => new
                        {
                            Products = pd,
                            TypeProducts = tpd,
                        }).SelectMany(
                          c => c.TypeProducts.DefaultIfEmpty(),
                          (pd, tpd) => new
                          {
                              pd.Products,
                              TypeProducts = tpd
                          }
                        );

            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.Products.Name.Contains(filter.sSearch));
            }

            switch (filter.Sort)
            {
                case SortFieldProduct.DateCreated:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DateCreated) : result.OrderByDescending(x => x.Products.DateCreated);
                    break;
                case SortFieldProduct.Name:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.Name) : result.OrderByDescending(x => x.Products.Name);
                    break;
                case SortFieldProduct.Status:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.Status) : result.OrderByDescending(x => x.Products.Status);
                    break;
                case SortFieldProduct.DateModify:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DateModified) : result.OrderByDescending(x => x.Products.DateModified);
                    break;
                case SortFieldProduct.DatePublished:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DatePublished) : result.OrderByDescending(x => x.Products.DatePublished);
                    break;
                case SortFieldProduct.Order:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.Order) : result.OrderByDescending(x => x.Products.Order);
                    break;
                default:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DateCreated) : result.OrderByDescending(x => x.Products.DateCreated);
                    break;
            }
            total = result.Count();
            var data = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength).Select(x => new { x.Products.Id, x.Products.ParentId, x.Products.Image, x.Products.Name, x.Products.DateCreated, x.Products.DateModified, x.Products.DatePublished, Price = x.TypeProducts != null ? x.TypeProducts.DiscountPrice : 0, x.Products.Status, x.Products.ViewCount }).ToList();

            var ICat = this.GetListParent(data.Select(x => x.ParentId).Distinct().ToList());

            return data.Select(x => new LProductVM
            {
                Id = x.Id,
                Category = ICat.ContainsKey(x.ParentId) ? ICat[x.ParentId] : "Chưa chọn",
                Image = CORE.UTILITY.MUtility.LoadImage(x.Image),
                Name = x.Name,
                Price = CORE.UTILITY.MUtility.FomatMoney(x.Price),
                Status = UiHelper.ShowHtmlStatus(x.Status),
                Date = $"<p>Ngày tạo: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày sửa: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày XB: {x.DatePublished.ToString("dd/MM/yyyy HH:mm")}</p>",
                Action = UiHelper.ShowHtmlAction(x.Id, x.Status),

            }).ToList();
        }

       

        public List<LArticleVM> LoadDataNews(Filter filter, TypeNews typeNews, TypeContent type, out int total)
        {
            total = 0;

            var result = this.FindAll(x => x.Type == type && x.TypePost == typeNews);

            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.Name.Contains(filter.sSearch));
            }
            switch (filter.Sort)
            {
                case SortFieldProduct.DateCreated:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.DateCreated) : result.OrderByDescending(x => x.DateCreated);
                    break;
                case SortFieldProduct.Name:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Name) : result.OrderByDescending(x => x.Name);
                    break;
                case SortFieldProduct.Status:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Status) : result.OrderByDescending(x => x.Status);
                    break;
                case SortFieldProduct.DateModify:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.DateModified) : result.OrderByDescending(x => x.DateModified);
                    break;
                case SortFieldProduct.DatePublished:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.DatePublished) : result.OrderByDescending(x => x.DatePublished);
                    break;
                case SortFieldProduct.Order:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Order) : result.OrderByDescending(x => x.Order);
                    break;
                default:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.DateCreated) : result.OrderByDescending(x => x.DateCreated);
                    break;
            }
            total = result.Count();
            var data = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength).Select(x => new { x.Id, x.ParentId, x.Image, x.Name, x.DateCreated, x.DateModified, x.DatePublished, x.Status }).ToList();

            var ICat = this.GetListParent(data.Select(x => x.ParentId).Distinct().ToList());

            return data.Select(x => new LArticleVM
            {
                Id = x.Id,
                Category = ICat.ContainsKey(x.ParentId) ? ICat[x.ParentId] : "Chưa chọn",
                Image = CORE.UTILITY.MUtility.LoadImage(x.Image),
                Name = x.Name,
                Status = UiHelper.ShowHtmlStatus(x.Status),
                Date = $"<p>Ngày tạo: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày sửa: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày XB: {x.DatePublished.ToString("dd/MM/yyyy HH:mm")}</p>",
                Action = UiHelper.ShowHtmlAction(x.Id, x.Status),
            }).ToList();
        }
        public List<LRecruitmentVM> LoadDataRecruitment(Filter filter, TypeNews typeNews, TypeContent type, out int total)
        {
            total = 0;

            var result = this.FindAll(x => x.Type == type && x.TypePost == typeNews)
                        .GroupJoin(this._context.TypeRecruitments, a => a.Id, b => b.Id,
                        (pd, tpd) => new
                        {
                            Products = pd,
                            TypeRecruitments = tpd,
                        }).SelectMany(
                          c => c.TypeRecruitments.DefaultIfEmpty(),
                          (pd, tpd) => new
                          {
                              pd.Products,
                              TypeRecruitments = tpd
                          }
                        );
            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.Products.Name.Contains(filter.sSearch));
            }
            switch (filter.Sort)
            {
                case SortFieldProduct.DateCreated:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DateCreated) : result.OrderByDescending(x => x.Products.DateCreated);
                    break;
                case SortFieldProduct.Name:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.Name) : result.OrderByDescending(x => x.Products.Name);
                    break;
                case SortFieldProduct.Status:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.Status) : result.OrderByDescending(x => x.Products.Status);
                    break;
                case SortFieldProduct.DateModify:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DateModified) : result.OrderByDescending(x => x.Products.DateModified);
                    break;
                case SortFieldProduct.DatePublished:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DatePublished) : result.OrderByDescending(x => x.Products.DatePublished);
                    break;
                case SortFieldProduct.Order:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.Order) : result.OrderByDescending(x => x.Products.Order);
                    break;
                default:
                    result = filter.SortDir == SortDir.ASC ? result.OrderBy(x => x.Products.DateCreated) : result.OrderByDescending(x => x.Products.DateCreated);
                    break;
            }
            total = result.Count();
            var data = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength).Select(x => new { x.Products.Id, x.Products.ParentId, x.Products.Image, x.Products.Name, x.Products.DateCreated, x.Products.DateModified, x.Products.DatePublished, Info = x.TypeRecruitments != null ? $"<p>Lương: {x.TypeRecruitments.Salary}</p><p>Hạn nộp: {x.TypeRecruitments.Deadline.ToString("dd/MM/yyyy HH:mm")}</p>" : "", x.Products.Status, x.Products.ViewCount }).ToList();

            var ICat = this.GetListParent(data.Select(x => x.ParentId).Distinct().ToList());

            return data.Select(x => new LRecruitmentVM
            {
                Id = x.Id,
                Category = ICat.ContainsKey(x.ParentId) ? ICat[x.ParentId] : "Chưa chọn",
                Image = CORE.UTILITY.MUtility.LoadImage(x.Image),
                Name = x.Name,
                Info = x.Info,
                Status = UiHelper.ShowHtmlStatus(x.Status),
                Date = $"<p>Ngày tạo: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày sửa: {x.DateCreated.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày XB: {x.DatePublished.ToString("dd/MM/yyyy HH:mm")}</p>",
                Action = UiHelper.ShowHtmlAction(x.Id, x.Status),

            }).ToList();
        }
        #endregion
        #region Add
        public bool AddProduct(KeyValuePair<Product, TypeProduct> obj)
        {
            var result = false;
            using (IDbContextTransaction transaction = this._context.Database.BeginTransaction())
            {

                var product = obj.Key;
                var tProduct = obj.Value;
                if (product.Id <= 0)
                {
                    try
                    {
                        bool isSuccess = this.Add(product, true);
                        if (isSuccess)
                        {
                            tProduct.Id = product.Id;
                            this._context.TypeProducts.Add(tProduct);
                        }
                        this.Commit();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    try
                    {
                        this.Update(product);
                        if (this._context.TypeProducts.Any(x => x.Id == tProduct.Id))
                        {
                            this._context.TypeProducts.Update(tProduct);
                        }
                        else
                        {
                            this._context.TypeProducts.Add(tProduct);
                        }
                        this.Commit();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    { }
                }
            }
            return result;
        }
        public bool AddProduct(KeyValuePair<Product, TypeRecruitment> obj)
        {
            var result = false;

            using (IDbContextTransaction transaction = this._context.Database.BeginTransaction())
            {
                var product = obj.Key;
                var tProduct = obj.Value;
                if (product.Id <= 0)
                {
                    try
                    {
                        this.Add(product, true);
                        tProduct.Id = product.Id;
                        this._context.TypeRecruitments.Add(tProduct);
                        this.Commit();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    try
                    {
                        this.Update(product);
                        if (this._context.TypeRecruitments.Any(x => x.Id == tProduct.Id))
                        {
                            this._context.TypeRecruitments.Update(tProduct);
                        }
                        else
                        {
                            this._context.TypeRecruitments.Add(tProduct);
                        }
                        this.Commit();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    { }
                }

            }
            return result;
        }

        public bool AddProduct(Product obj)
        {
            bool result = false;
            try
            {
                if (obj.Id <= 0)
                {
                    result = this.Add(obj, true);
                }
                else
                {
                    this.Update(obj, true);
                }
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        #endregion


        public Dictionary<int, string> GetListParent(List<int> lstId)
        {
            return this.FindAll(x => lstId.Contains(x.Id), new string[] { "Id", "Name" }).ToDictionary(x => x.Id, x => x.Name);
        }

        public bool UnLock(Product obj)
        {
            bool result = false;
            try
            {
                this._context.Products.Attach(obj);
                var entry = this._context.Entry(obj);
                entry.Property(x => x.Status).IsModified = true;
                entry.Property(x => x.DateModified).IsModified = true;
                this.Commit();
                result = true;
            }
            catch (Exception ex)
            { }
            return result;
        }

        public Dictionary<int, string> GetNameById(List<int> lstId)
        {
            return this.FindAll(x => lstId.Contains(x.Id), new string[] { "Id", "Name" }).ToDictionary(x => x.Id, x => x.Name);
        }
    }
}
