﻿using CORE.APPLICATION.Interfaces;
using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using CORE.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class PromotionService : Repository<Promotion>, IPromotionService, IDisposable
    {
        public PromotionService(AppDbContext context) : base(context)
        {

        }

        public bool AddPromotion(Promotion obj)
        {
            bool result = false;
            try
            {
                if (!String.IsNullOrEmpty(obj.Id))
                {
                    this.Update(obj, true);
                }
                else
                {
                    obj.Id = MUtility.TimeEpochNow();
                    this.Add(obj, true);
                }
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public List<LPromotionVM> LoadData(FilterString filter, out int total)
        {
            total = 0;

            var result = this.FindAll(x => x.AppId == filter.AppId);

            if (!String.IsNullOrEmpty(filter.sSearch))
            {
                result = result.Where(x => x.Name.Contains(filter.sSearch) || x.Id.Equals(filter.sSearch));
            }
            if (String.IsNullOrEmpty(filter.Sort))
            {
                filter.Sort = "DateCreated";
            }
            result = filter.SortDir == DATA.Enums.SortDir.ASC ? result.OrderBy(filter.Sort) : result.OrderByDescending(filter.Sort);

            total = result.Count();
            var data = result.Skip(filter.iDisplayStart).Take(filter.iDisplayLength).Select(x => new { x.Id, x.Name, x.StartDate, x.EndDate, x.Status }).ToList();
            return data.Select(x => new LPromotionVM
            {
                Id = x.Id,
                Name = x.Name,
                Status = UiHelper.ShowHtmlStatus(x.Status),
                DateInfo = $"<p>Ngày BĐ: {x.StartDate.ToString("dd/MM/yyyy HH:mm")}</p><p>Ngày KT: {x.EndDate.ToString("dd/MM/yyyy HH:mm")}</p>",
                Action = UiHelper.ShowHtmlAction(x.Id, x.Status),
            }).ToList();
        }
    }
}
