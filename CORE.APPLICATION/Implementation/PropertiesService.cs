﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class PropertiesService : Repository<Property>, IPropertiesService, IDisposable
    {
        public PropertiesService(AppDbContext context) : base(context)
        {

        }
        public bool AddProperty(Property obj)
        {
            bool result = false;
            try
            {
                if (this._context.Properties.Any(x => x.Id == obj.Id))
                {
                    this.Update(obj, true);
                }
                else
                {
                    this.Add(obj, true);
                }
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

       
    }
}
