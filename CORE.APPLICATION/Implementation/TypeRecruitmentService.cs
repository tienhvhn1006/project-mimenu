﻿using CORE.DATA.EF;
using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public class TypeRecruitmentService : Repository<TypeRecruitment>, ITypeRecruitmentService
    {
        public TypeRecruitmentService(AppDbContext context) : base(context)
        {

        }
    }
}
