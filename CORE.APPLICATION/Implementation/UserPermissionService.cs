﻿using CORE.APPLICATION.Interfaces;
using CORE.DATA.EF;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Implementation
{
    public class UserPermissionService : Repository<UserPermission>, IUserPermissionService, IDisposable
    {
        public UserPermissionService(AppDbContext context) : base(context)
        {

        }
    }
}
