﻿using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IAdvertisementsService : IRepository<Advertisement>, IDisposable
    {
        public bool AddAds(Advertisement obj);
    }
}
