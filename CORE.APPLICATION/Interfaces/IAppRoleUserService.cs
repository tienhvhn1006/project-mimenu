﻿using CORE.INFRASTRUCTURE.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IAppRoleUserService: IRepository<IdentityUserRole<Guid>>, IDisposable
    {
        public bool AddMutil(List<Guid> lstRole,Guid userId);
        
    }
}
