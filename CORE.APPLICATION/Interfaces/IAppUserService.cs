﻿using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CORE.APPLICATION.Interfaces
{
    public interface IAppUserService: IRepository<AppUser>, IDisposable
    {
        bool UnLock(AppUser appUser);

        public List<ViewModels.LAppUserVM> LoadData(FilterString filter,  out int total);
        public bool UpdateInfo(AppUser appUser,List<Guid> lstRole);

        Task GenerateForgotPasswordTokenAsync(AppUser user);

        Task<IdentityResult> ResetPasswordAsync(ResetPasswordVM model);
    }
}
