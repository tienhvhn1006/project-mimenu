﻿using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;

using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IColorService : IRepository<Color>, IDisposable
    {
        public bool AddColor(Color obj);
    }
}
