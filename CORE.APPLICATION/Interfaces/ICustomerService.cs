﻿using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface ICustomerService : IRepository<Customer>, IDisposable
    {
        public List<CustomerVM> LoadData(FilterStringCustomer filter, out int total);

        public bool AddOrUpdate(Customer obj);
    }
}
