﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IHelperUpload
    {
        public string UploadImage(string PathRoot, string AppId, List<IFormFile> files);
    }
}
