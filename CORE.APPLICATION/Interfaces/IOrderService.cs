﻿using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IOrderService : IRepository<Order>, IDisposable
    {
        public List<LOrderVM> LoadData(FilterStringOrder filter, out int total);
        public Order GetById(int orderId);
        public bool UpdateDeleted(int orderId);

        public bool UpdateStatus(Order order);

        public Dictionary<int, int> StatisticsByStatus(DateTime dateFrom, DateTime dateTo, string appId);

        public List<KeyValuePair<string, string>> Top5Sale(DateTime dateFrom, DateTime dateTo, string appId);
        public List<KeyValuePair<string, string>> Top5Revenue(DateTime dateFrom, DateTime dateTo, string appId);

        public List<Order> Top5Order(string appId);

        public Dictionary<string, int> StatisticalByMonth(DateTime dateFrom,string appId);
        public Dictionary<string, int> StatisticalByWeek(DateTime dateFrom, string appId);
        public Dictionary<string, int> StatisticalByYear(DateTime dateFrom, string appId);


        public Dictionary<string, double> StatisticalRevenueByMonth(DateTime dateFrom, string appId);

        public Dictionary<string, double> StatisticalRevenueByWeek(DateTime dateFrom, string appId);
        public Dictionary<string, double> StatisticalRevenueByYear(DateTime dateFrom, string appId);

    }
}
