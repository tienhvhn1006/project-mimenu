﻿using CORE.APPLICATION.Query;
using CORE.DATA.Entities;
using CORE.DATA.Enums;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IProductService : IRepository<Product>, IDisposable
    {
        public List<ViewModels.LProductVM> LoadData(Filter filter, TypeContent type, out int total);
        public List<ViewModels.LArticleVM> LoadDataNews(Filter filter, TypeNews typeNews, TypeContent type, out int total);
        public List<ViewModels.LRecruitmentVM> LoadDataRecruitment(Filter filter, TypeNews typeNews, TypeContent type, out int total);

        public bool AddProduct(KeyValuePair<Product, TypeProduct> obj);
        public bool AddProduct(KeyValuePair<Product, TypeRecruitment> obj);

        public Dictionary<int, string> GetNameById(List<int> lstId);
        public bool AddProduct(Product obj);
        public bool Delete(int id);
        public bool DeleteZone(int id);
        public bool DeleteArticle(int id);

        public bool UnLock(Product obj);

    }
}
