﻿using CORE.APPLICATION.Query;
using CORE.APPLICATION.ViewModels;
using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IPromotionService : IRepository<Promotion>, IDisposable
    {

        public List<ViewModels.LPromotionVM> LoadData(FilterString filter, out int total);
        public bool AddPromotion(Promotion obj);
    }
}
