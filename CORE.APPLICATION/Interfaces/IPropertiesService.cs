﻿using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IPropertiesService : IRepository<Property>, IDisposable
    {
        public bool AddProperty(Property obj);
    }
}
