﻿using CORE.DATA.Entities;
using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Interfaces
{
    public interface IUserPermissionService : IRepository<UserPermission>, IDisposable
    {
    }
}
