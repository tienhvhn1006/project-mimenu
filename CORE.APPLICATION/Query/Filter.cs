﻿using CORE.DATA.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Query
{
    public class Filter
    {
        public SortDir SortDir { get; set; }
        public SortFieldProduct Sort { get; set; }
        public string sSearch { get; set; }
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public string AppId { get; set; }
        public Filter()
        {
            this.SortDir = DATA.Enums.SortDir.DESC;
            this.Sort = SortFieldProduct.DateCreated;
            this.sSearch = string.Empty;
            this.iDisplayLength = 10;
            this.iDisplayStart = 0;
            this.AppId = string.Empty;
        }
    }

    public class FilterString
    {
        public SortDir SortDir { get; set; }
        public string Sort { get; set; }
        public string sSearch { get; set; }
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public string AppId { get; set; }
        public FilterString()
        {
            this.SortDir = DATA.Enums.SortDir.DESC;
            this.Sort = "";
            this.sSearch = string.Empty;
            this.iDisplayLength = 10;
            this.iDisplayStart = 0;
            this.AppId = string.Empty;
        }
    }

    public class FilterStringOrder
    {
        public int iSortCol_0 { get; set; }
        public string sSortDir_0 { get; set; }
        public string sSearch { get; set; }
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public StatusOrder Status { get; set; }
        public string AppId { get; set; }
        public FilterStringOrder()
        {
            this.sSortDir_0 = "DESC";
            this.iSortCol_0 = 0;
            this.sSearch = string.Empty;
            this.iDisplayLength = 10;
            this.iDisplayStart = 0;
            this.Status = StatusOrder.All;
            this.AppId = "";
        }
    }

    public class FilterStringCustomer
    {
        public int iSortCol_0 { get; set; }
        public string sSortDir_0 { get; set; }
        public string sSearch { get; set; }
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public TypeCustomer Type { get; set; }
        public string AppId { get; set; }
        public FilterStringCustomer()
        {
            this.sSortDir_0 = "DESC";
            this.iSortCol_0 = 0;
            this.sSearch = string.Empty;
            this.iDisplayLength = 10;
            this.iDisplayStart = 0;
            this.Type =  TypeCustomer.All;
            this.AppId = "";
        }
    }
}
