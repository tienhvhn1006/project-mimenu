﻿
using CORE.DATA.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Query
{
    public class ParseJson
    {
        public static CORE.UTILITY.Object.PromotionInfo ParsePromotion(string json)
        {
            try
            {
                var obj = JsonConvert.DeserializeObject<CORE.UTILITY.Object.PromotionInfo>(json);
                if(obj == null)
                {
                    return new CORE.UTILITY.Object.PromotionInfo();
                } 
            }
            catch (Exception ex)
            { }
            return new CORE.UTILITY.Object.PromotionInfo();
        }
        public static List<Block> ParseBlock(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<List<Block>>(json);
            }
            catch (Exception ex)
            { 
            }
            return new List<Block>();
        }
    }
}
