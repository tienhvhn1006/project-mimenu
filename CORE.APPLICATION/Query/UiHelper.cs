﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.Query
{
    public class UiHelper
    {
        public static string ShowHtmlStatus(DATA.Enums.Status status)
        {
            StringBuilder sb = new StringBuilder();
            if (status == DATA.Enums.Status.Active)
            {
                sb.Append("<div class=\"chip chip-success mr-1\"><div style='min-width:70px' class=\"chip-body\"><div class=\"avatar\"><i  class=\"feather icon-edit\"></i></div><span class=\"chip-text\">Hiển thị</span></div></div>");
            }
            else
            {
                sb.Append("<div class=\"chip chip-danger mr-1\"><div style='min-width:70px' class=\"chip-body\"><div class=\"avatar\"><i  class=\"feather icon-slash\"></i></div><span class=\"chip-text\">Ẩn </span></div></div>");
            }
            return sb.ToString();
        }


        public static string ShowHtmlStatusOrder(DATA.Enums.StatusOrder status,int id)
        {
            StringBuilder sb = new StringBuilder();
            if (status == DATA.Enums.StatusOrder.New)
                sb.Append($"<span data-id=\"{id}\" id=\"stod-{id}\"> <span  title='Đơn mới' class=\"badge badge-primary   mr-1 mb-1\"><i  class=\"feather icon-alert-circle icon-status\"></i><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(status)}</span></span></span>");
            if (status == DATA.Enums.StatusOrder.Process)
                sb.Append($"<span data-id=\"{id}\" id=\"stod-{id}\"><span   title='Đang xử lý' class=\"badge badge-warning mr-1 mb-1\"><i  class=\"feather icon-refresh-ccw icon-status\"></i><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(status)}</span></span></span>");
            if (status == DATA.Enums.StatusOrder.Success)
                sb.Append($"<span data-id=\"{id}\" id=\"stod-{id}\"><span   title='Thành công' class=\"badge badge-success mr-1 mb-1\"><i  class=\"feather icon-check-circle icon-status\"></i><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(status)}</span></span></span>");
            if (status == DATA.Enums.StatusOrder.Cancel)
                sb.Append($"<span data-id=\"{id}\" id=\"stod-{id}\"><span   title='Hủy đơn' class=\"badge badge-danger mr-1 mb-1\"><i  class=\"feather icon-x-circle icon-status\"></i><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(status)}</span></span></span>");
            return sb.ToString();

        }
        public static string ShowHtmlTypeCustomer(DATA.Enums.TypeCustomer type)
        {
            StringBuilder sb = new StringBuilder();
            if (type ==  DATA.Enums.TypeCustomer.Popular)
                sb.Append($"<span > <span  title='Phổ biến' class=\"badge badge-primary   mr-1 mb-1\"><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(type)}</span></span></span>");
            if (type == DATA.Enums.TypeCustomer.Potential)
                sb.Append($"<span ><span   title='Tiềm năng' class=\"badge badge-warning mr-1 mb-1\"><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(type)}</span></span></span>");
            if (type == DATA.Enums.TypeCustomer.VIP)
                sb.Append($"<span ><span   title='Khách VIP' class=\"badge badge-success mr-1 mb-1\"><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(type)}</span></span></span>");
            if (type == DATA.Enums.TypeCustomer.Backlist)
                sb.Append($"<span ><span   title='Black List' class=\"badge badge-danger mr-1 mb-1\"><span class='txt-status'>{CORE.DATA.EnumHelper.GetDescription(type)}</span></span></span>");
            return sb.ToString();

        }
        public static string ShowHtmlStatusOrderHome(DATA.Enums.StatusOrder status)
        {
            StringBuilder sb = new StringBuilder();
            if (status == DATA.Enums.StatusOrder.New)
                sb.Append("<div class=\"avatar bg-rgba-primary p-50 m-0\"> <div class=\"avatar-content\"> <i class=\"feather icon-alert-circle text-primary font-medium-5\"></i> </div> </div>");
            if (status == DATA.Enums.StatusOrder.Process)
                sb.Append("<div class=\"avatar bg-rgba-warning p-50 m-0\"> <div class=\"avatar-content\"> <i class=\"feather icon-refresh-ccw text-warning font-medium-5\"></i> </div> </div>");
            if (status == DATA.Enums.StatusOrder.Success)
                sb.Append(" <div class=\"avatar bg-rgba-success p-50 m-0\"> <div class=\"avatar-content\"> <i class=\"feather icon-check-circle text-success font-medium-5\"></i> </div> </div>");
            if (status == DATA.Enums.StatusOrder.Cancel)
                sb.Append(" <div class=\"avatar bg-rgba-danger p-50 m-0\"> <div class=\"avatar-content\"> <i class=\"feather icon-x-circle text-danger font-medium-5\"></i> </div> </div>");
            return sb.ToString();

        }


        public static string ShowTxtThuocTinh(int thuocTinh)
        {

            return $"#{thuocTinh}#".ToString();

        }
        public static int ShowIntStatus(DATA.Enums.Status status)
        {
            return (int)status;

        }



        public static string ShowHtmlAction(object id, DATA.Enums.Status status)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='act-table'>");
            sb.Append($"<span title='Sửa bản ghi' data-id=\"{id}\" class=\"action-edit\"><i data-id=\"{id}\" class=\"feather icon-edit\"></i></span> ");

            sb.Append($"<span title='Xóa bản ghi' data-id=\"{id}\" class=\"action-delete\"><i data-id=\"{id}\" class=\"feather icon-trash \"></i></span>");
            if (status == DATA.Enums.Status.Active)
            {
                sb.Append($"<span title='Ẩn bài viết' data-id=\"{id}\" class=\"action-lock\"><i data-id=\"{id}\" class=\"feather icon-unlock \"></i></span>");
            }
            else
            {
                sb.Append($"<span title='Hiển thị' data-id=\"{id}\" class=\"action-unlock\"><i data-id=\"{id}\" class=\"feather icon-lock \"></i></span>");
            }
            sb.Append("</div>");
            return sb.ToString();

        }
        public static string ShowHtmlAction(object id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='act-table'>");
            sb.Append($"<span title='Sửa bản ghi' data-id=\"{id}\" class=\"action-edit\"><i data-id=\"{id}\" class=\"feather icon-edit\"></i></span> ");

            sb.Append($"<span title='Xóa bản ghi' data-id=\"{id}\" class=\"action-delete\"><i data-id=\"{id}\" class=\"feather icon-trash \"></i></span>");

            sb.Append("</div>");
            return sb.ToString();

        }
        public static string ShowHtmlActionEdit(object id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='act-table'>");
            sb.Append($"<span title='Sửa bản ghi' data-id=\"{id}\" class=\"action-edit\"><i data-id=\"{id}\" class=\"feather icon-edit\"></i></span> ");

            //sb.Append($"<span title='Xóa bản ghi' data-id=\"{id}\" class=\"action-delete\"><i data-id=\"{id}\" class=\"feather icon-trash \"></i></span>");

            sb.Append("</div>");
            return sb.ToString();

        }
        public static string ShowHtmlActionOrder(object id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='act-table'>");
            sb.Append($"<span title='Sửa bản ghi' data-id=\"{id}\" class=\"action-edit\"><i data-id=\"{id}\" class=\"feather icon-edit\"></i></span> ");
            sb.Append($"<span title='Xem bản ghi' data-id=\"{id}\" class=\"action-view\"><i data-id=\"{id}\" class=\"feather icon-eye\"></i></span> ");
            sb.Append($"<span title='Xóa bản ghi' data-id=\"{id}\" class=\"action-delete\"><i data-id=\"{id}\" class=\"feather icon-trash \"></i></span>");

            sb.Append("</div>");
            return sb.ToString();

        }
        public static string ShowHtmlActionAccount(object id, DATA.Enums.Status status)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='act-table'>");
            sb.Append($"<span title='Sửa tải khoản' data-id=\"{id}\" class=\"action-edit\"><i data-id=\"{id}\" class=\"feather icon-edit\"></i></span> ");
            sb.Append($"<span title='Phân quyền' data-id=\"{id}\" class=\"action-edit-per\"><i data-id=\"{id}\" class=\"feather icon-user-check\"></i></span> ");
            if (status == DATA.Enums.Status.Active)
            {
                sb.Append($"<span title='Khóa tài khoản' data-id=\"{id}\" class=\"action-lock\"><i data-id=\"{id}\" class=\"feather icon-unlock \"></i></span>");
            }
            else
            {
                sb.Append($"<span title='Mở khóa tài khoản' data-id=\"{id}\" class=\"action-unlock\"><i data-id=\"{id}\" class=\"feather icon-lock \"></i></span>");
            }
            sb.Append("</div>");

            return sb.ToString();

        }
    }
}
