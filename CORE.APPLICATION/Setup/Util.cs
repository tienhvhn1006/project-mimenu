﻿using CORE.APPLICATION.Query;
using CORE.DATA.Entities;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CORE.APPLICATION.Setup
{
    public class Util
    {
        public static List<TreeModel> BuildTree()
        {
            return new List<TreeModel>();
        }

        public static List<TreeModel> GetTree(List<Product> list, int parent)
        {
            return list.Where(x => x.ParentId == parent).OrderBy(x => x.Order).Select(x => new TreeModel
            {
                Id = x.Id,
                Name = x.Name,
                Status = UiHelper.ShowHtmlStatus(x.Status),
                Child = x.ParentId != x.Id ? GetTree(list, x.Id) : new List<TreeModel>()
            }).ToList();
        }

        public static List<TreeModelMenu> GetTreeMenu(List<Function> list, string parent)
        {
            return list.Where(x => x.ParentId == parent).OrderBy(x => x.Base).Select(x => new TreeModelMenu
            {
                Id = x.Id,
                Name = x.Name,
                Url = x.URL,
                Icon = x.IconCss,
                Child = x.ParentId != x.Base ? GetTreeMenu(list, x.Base) : new List<TreeModelMenu>()
            }).ToList();
        }


        public static MemoryCacheEntryOptions OptionsCache(int aExpiration, int sExpiration)
        {
            return new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(aExpiration),
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(sExpiration),
                Size = 1024,
            };
        }
    }
}
