﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class CustomerVM
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Type { get; set; }
        public string Birthday { get; set; }
        //public string LastActive { get; set; }
        public string Action { get; set; }
        public CustomerVM()
        {
            this.Id = 0;
            this.Name = string.Empty;
            this.Phone = string.Empty;
            this.Type = string.Empty;
            this.Birthday = string.Empty;
            this.Action = string.Empty;
        }
    }
}
