﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class FieldTable
    {
        public string Name { get; set; } = "";
        public string Title { get; set; } = "";
        public bool Sort { get; set; } = false;
        public string Class { get; set; } = "";
    }
}
