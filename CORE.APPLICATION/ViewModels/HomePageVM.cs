﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class HomePageVM
    {
        public Dictionary<int, int> StatusOrder { get; set; } = new Dictionary<int, int>();

        public List<KeyValuePair<string, string>> Top5Sale { get; set; } = new List<KeyValuePair<string, string>>();
        public List<CORE.DATA.Entities.Order> Top5Order { get; set; } = new List<CORE.DATA.Entities.Order>();
        public List<int> StatisticalCountMonth { get; set; } = new List<int>();
        public List<double> StatisticalRevenueMonth { get; set; } = new List<double>();
    }
}
