﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class LAppUserVM
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int AppId { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }
        public string Action { get; set; }
        //public List<Guid> Roles { get; set; }
    }
}
