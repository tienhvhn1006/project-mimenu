﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class LArticleVM
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public int ParrentId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }


    }
}
