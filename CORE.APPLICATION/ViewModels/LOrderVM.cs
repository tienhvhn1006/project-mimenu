﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class LOrderVM
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }
        public string InfoUser { get; set; }
        public string Action { get; set; }
    }
}
