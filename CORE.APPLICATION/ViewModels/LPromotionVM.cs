﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class LPromotionVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DateInfo { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }
    }
}
