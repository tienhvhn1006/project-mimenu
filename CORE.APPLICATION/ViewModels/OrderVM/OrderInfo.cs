﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class OrderInfo
    {
        public int Id { get; set; }
        public int IdQrCode { get; set; }
        public string IdPromotion { get; set; }
        public string Code { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public DATA.Enums.StatusOrder Status { get; set; }
        public List<ProductVM> Products { get; set; }
    }

    public class ProductVM
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
        public double total { get; set; }

    }
}
