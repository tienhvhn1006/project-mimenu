﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.APPLICATION.ViewModels
{
    public class PermissionVM
    {
        public List<Actions> Actions { get; set; }
        public Guid Id { get; set; }
    }
    public class Actions
    {
        public string FunctionId { get; set; } = "";
        public DATA.Enums.Action Action { get; set; } = DATA.Enums.Action.Write;
        public bool IsEdit { get; set; } = false;
    }
}
