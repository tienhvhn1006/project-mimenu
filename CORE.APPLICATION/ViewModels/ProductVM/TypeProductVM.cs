﻿using AutoMapper;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace CORE.APPLICATION.ViewModels
{
    public class TypeProductVM : Product
    {
        public int Quantity { get; set; } = 0;
        public int Unit { get; set; } = 1;
        public string AvatarArray { get; set; } = "";
        public double Price { get; set; } = 0;
        public double DiscountPercent { get; set; } = 0;
        public double DiscountPrice { get; set; } = 0;
        public bool VAT { get; set; } = false;
        public int BrandId { get; set; } = 0;

        public List<string> Colors { get; set; } = new List<string>();

        public string BuyTogether { get; set; } = ""; //Sản phẩm mua kèm 
        public KeyValuePair<Product, TypeProduct> Init()
        {
            var json = JsonSerializer.Serialize(this);

            var product = JsonSerializer.Deserialize<Product>(json);

            product.SetDefault();

            var typeP = JsonSerializer.Deserialize<TypeProduct>(json);
            typeP.ColorId = UTILITY.MUtility.JoinStr(this.Colors);
            return new KeyValuePair<Product, TypeProduct>(product, typeP);
        }
    }



}
