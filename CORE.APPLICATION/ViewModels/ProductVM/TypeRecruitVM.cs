﻿using AutoMapper;
using CORE.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace CORE.APPLICATION.ViewModels
{
    public class TypeRecruitVM : Product
    {
        public DateTime Deadline { get; set; } = DateTime.Now;
        public string WorkingForm { get; set; } = "";
        public string Salary { get; set; } = "";
        public string Place { get; set; } = "";
        public string Business { get; set; } = "";
        public string Position { get; set; } = "";

        public KeyValuePair<Product, TypeRecruitment> Init()
        {
            var json = JsonSerializer.Serialize(this);
           
            var product = JsonSerializer.Deserialize<Product>(json);
            var typeP = JsonSerializer.Deserialize<TypeRecruitment>(json);

            return new KeyValuePair<Product, TypeRecruitment>(product, typeP);


        }
    }

    

}
