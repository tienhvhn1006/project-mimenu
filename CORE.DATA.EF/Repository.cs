﻿using CORE.INFRASTRUCTURE.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CORE.DATA.EF
{
    public class Repository<T> : IRepository<T>, IDisposable where T : class
    {
        protected readonly AppDbContext _context;

        public Repository(AppDbContext context)
        {
            _context = context;
        }
        #region Query
        public bool Add(T entity, bool save = false)
        {
            var result = false;
            try
            {
                _context.Add(entity);
                if (save)
                {
                    this.Commit();
                }
                result = true;
            }
            catch(Exception ex)
            {
            }
            return result;
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public IQueryable<T> FindAll(string[] fields = null)
        {
            IQueryable<T> items = null;
            if (fields != null && fields.Length > 0)
            {
                items = _context.Set<T>().SelectMembers(fields);
            }
            else
            {
                items = _context.Set<T>();
            }
            return items;
        }

        public IQueryable<T> FindAll(Expression<Func<T, bool>> predicate, string[] fields = null)
        {
            IQueryable<T> items = null;
            if (fields != null && fields.Length > 0)
            {
                items = _context.Set<T>().Where(predicate).SelectMembers(fields);
            }
            else
            {
                items = _context.Set<T>().Where(predicate);
            }
            return items;
        }
        public bool IsExist(Expression<Func<T, bool>> predicate)
        {
            bool result = false;
            result = _context.Set<T>().Any(predicate);
            return result;
        }
        public T FindById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public T FindSingle(Expression<Func<T, bool>> predicate)
        {
            return FindAll().SingleOrDefault(predicate);
        }

        public bool Remove(T entity, bool save = false)
        {
            var result = false;
            try
            {
                _context.Set<T>().Remove(entity);
                if (save)
                {
                    this.Commit();
                }
                result = true;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public bool Remove(int id, bool save = false)
        {
            var result = false;
            try
            {
                var entity = FindById(id);
                Remove(entity);
                if (save)
                {
                    this.Commit();
                }
                result = true;
            }
            catch (Exception)
            {

            }
            return result;

        }

        public bool RemoveMultiple(List<T> entities, bool save = false)
        {
            var result = false;
            try
            {
                _context.Set<T>().RemoveRange(entities);
                if (save)
                {
                    this.Commit();
                }
                result = true;
            }
            catch
            {

            }
            return result;
        }

        public bool Update(T entity, bool save = false)
        {
            var result = false;
            try
            {
                _context.Set<T>().Update(entity);
                if (save)
                {
                    this.Commit();
                }
                result = true;
            }
            catch
            {

            }
            return result;
        }
        public void Commit()
        {
            _context.SaveChanges();
        }

        public T FindById(string id)
        {
            return _context.Set<T>().Find(id);
        }
        #endregion

    }
}
public static partial class QueryableExtensions
{
    public static IQueryable<T> SelectMembers<T>(this IQueryable<T> source, params string[] memberNames)
    {
        var parameter = Expression.Parameter(typeof(T), "e");
        var bindings = memberNames
            .Select(name => Expression.PropertyOrField(parameter, name))
            .Select(member => Expression.Bind(member.Member, member));
        var body = Expression.MemberInit(Expression.New(typeof(T)), bindings);
        var selector = Expression.Lambda<Func<T, T>>(body, parameter);
        return source.Select(selector);
    }
    public static IQueryable<T> FilterText<T>(this IQueryable<T> queryable, IEnumerable<string> keywords)
    {
        var entityType = typeof(T);
        var parameter = Expression.Parameter(entityType, "a");
        var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        var propertyExpression = Expression.Property(parameter, "Text");
        Expression body = Expression.Constant(false);
        foreach (var keyword in keywords)
        {
            var innerExpression = Expression.Call(propertyExpression, containsMethod, Expression.Constant(keyword));
            body = Expression.OrElse(body, innerExpression);
        }
        var lambda = Expression.Lambda<Func<T, bool>>(body, new[] { parameter });
        return queryable.Where(lambda);

    }
}

