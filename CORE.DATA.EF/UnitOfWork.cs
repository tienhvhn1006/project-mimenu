﻿using CORE.INFRASTRUCTURE.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }
        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
