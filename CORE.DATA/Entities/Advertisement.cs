﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Advertisement
    {
        public int Id { get; set; }
        public string Code { get; set; } = "";
        public string Name { get; set; } = "";
        public string Info { get; set; } = "";
        public DateTime DateCreated { set; get; } = DateTime.Now;
        public DateTime DateModified { set; get; } = DateTime.Now;
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now.AddYears(2);
        public Enums.Status Status { get; set; } = Enums.Status.Active;
        public string AppId { get; set; } = "";
    }


    public class Block
    {
        public string Avatar { get; set; } = "";
        public string Name { get; set; } = "";
        public string Url { get; set; } = "";
        public string Description { get; set; } = "";
        public Enums.Status Status { get; set; } = Enums.Status.Active;
        public int Sort { get; set; } = 0;

    }
}
