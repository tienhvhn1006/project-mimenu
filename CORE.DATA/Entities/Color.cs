﻿using CORE.DATA.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Color
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public Status Status { get; set; }
    }
}
