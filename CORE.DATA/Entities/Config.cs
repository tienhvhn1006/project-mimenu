﻿using CORE.DATA.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Config
    {
        public Guid Id { get; set; }
        public string Key { get; set; } = "";
        public string Name { get; set; } = "";
        public string Value { get; set; } = "";
        public string Page { get; set; } = "";
        public string AppId { get; set; } = "";
        public TypeConfig Type { get; set; } = TypeConfig.Input;
    }
}
