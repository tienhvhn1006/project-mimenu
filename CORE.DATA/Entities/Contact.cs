﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Contact
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public string Address { get; set; } = "";
        public string Gender { get; set; } = "";
        public string Phone { get; set; } = "";
        public string Email { get; set; } = "";
        public string Title { get; set; } = "";
        public string Content { get; set; } = "";
        public string Note { get; set; } = "";
        public DateTime? CreatedDate { get; set; } = DateTime.Now;
        public string ModifiedBy { get; set; } = "";
        public DateTime? ModifiedDate { get; set; } = DateTime.Now;
        public Enums.StatusContact Status { get; set; } = Enums.StatusContact.New; // Xử lý xong chưa
        public string UrlRef { get; set; } = "";
    }
}
