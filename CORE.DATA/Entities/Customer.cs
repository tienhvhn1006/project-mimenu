﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; } = "";
        public string Email { get; set; } = "";
        public DateTime Birthday { get; set; } = new DateTime(1970, 01, 01);//Datetime mặc định
        public DateTime DateCreated { set; get; } = DateTime.Now;
        public DateTime DateModified { set; get; } = DateTime.Now;
        public string ModifiedBy { set; get; } = "";
        public DateTime LastActive { set; get; } = DateTime.Now;
        public string ActiveBy { set; get; } = "";
        public Enums.TypeCustomer TypeCustomer { get; set; } = Enums.TypeCustomer.Popular;
        public Enums.Status Status { get; set; } = Enums.Status.Active;
        public string AppId { get; set; } = "";
        public string Address { get; set; } = "";
    }
}
