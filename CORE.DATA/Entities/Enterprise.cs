﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Enterprise
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Avatar { get; set; } = "";
        public string Avatar2 { get; set; } = "";
    
        public string Address { get; set; } = "";
        public string Phone { get; set; } = "";
        public string Email { get; set; } = "";
        public string FaceBook { get; set; } = "";
        public string Twiter { get; set; } = "";
        public string Youtube { get; set; } = "";
        public string Linkedin { get; set; } = "";
        public DateTime EndDate { get; set; } = DateTime.Now.AddYears(1);
        public DateTime DateCreated { set; get; } = DateTime.Now;
        public DateTime DateModified { set; get; } = DateTime.Now;
        public string CreatedBy { get; set; } = "";
        public string ModifiedBy { get; set; } = "";
        public Enums.Status Status { get; set; } = Enums.Status.Active;
        public string AppId { get; set; } = "";
        public string Domain { get; set; } = "";

    }
}
