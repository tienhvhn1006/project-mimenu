﻿using CORE.DATA.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Order
    {
        public int Id { get; set; } = 0;
        public string Code { get; set; } = "";
        public bool VAT { get; set; } = false; //Hóa đơn này có lấy VAT hay không
        public int IdQRCode { get; set; } = 0;
        public string Ref { get; set; } = ""; //Chỗ này có thể note số bàn , hoặc 1 cái gì đó để đánh dấu
        public Enums.Transportation Trans { get; set; } = Transportation.ShipCode; //Hình thức vận chuyển
        public int CustomerId { get; set; } = 0;
        public string Name { get; set; } = "";
        public string Phone { get; set; } = "";
        public string Email { get; set; } = "";
        public string Address { get; set; } = "";
        public string Note { get; set; } = "";
        public int City { get; set; } = 0;
        public int District { get; set; } = 0;
        public double LogPrice { get; set; } = 0; // Số tiền tạm tính
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; } = "";
        public StatusOrder Status { get; set; } = StatusOrder.New;
        public string AppId { get; set; } = "";
        public bool IsDeleted { get; set; } = false;
        public string IdClient { get; set; } = "";//Lưu Id client => Khi người dùng vào Website thì sẽ sinh ra 1 mã unique ở cookie / Mục đích sử dụng để truy vấn lịch sử đơn hàng người dùng đặt
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
