﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CORE.DATA.Entities
{
    public class OrderDetail
    {
        [Key]
        public string Id { get; set; } = "";

        [ForeignKey("OrderId")]
        public int OrderId { get; set; } = 0;
        public int ProductId { get; set; } = 0;
        public double PriceProduct { get; set; } //Giá sản phẩm lúc mua
        public int Quantity { get; set; } = 0;
        public bool Vat { get; set; } = false;
        public double LogPrice { get; set; } = 0;//Số tiền tạm tính khi mua hàng lưu vào 1 trường về sau còn thống kê
        public string Note { get; set; } = "";//Note 
        public virtual Order Order { get; set; }
    }


}
