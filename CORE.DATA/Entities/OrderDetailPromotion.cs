﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CORE.DATA.Entities
{
    public class OrderDetailPromotion
    {
        [Key]
        public string Id { get; set; } = ""; //OrderDetailIdxPromotionId
        public int LogPrice { get; set; } = 0; // Số tiền được khuyến mại
        public string OrderDetailId { get; set; } = string.Empty;
        public int PromotionId { get; set; } = 0;
        public bool IsVoucher { get; set; } = false;
        public string LogInfo { get; set; } = ""; //Về sau để đối chiếu
    }
}
