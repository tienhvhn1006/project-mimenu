﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace CORE.DATA.Entities
{
    public class OrderPromotion
    {
        [Key]
        public string Id { get; set; } = ""; //OrderDetailIdxPromotionId
        public string Code { get; set; } = ""; //Lưu Code tránh tình trạng người dùng thay đôi
        public int LogPrice { get; set; } = 0; // Số tiền được khuyến mại
        public int OrderId { get; set; } = 0;
        public int PromotionId { get; set; } = 0;
        public string LogInfo { get; set; } = ""; //Về sau để đối chiếu  (Lưu:Discounts,TypeMaxValue) //Xem object  CORE.UTILITY.Object.PromotionInfo.cs
    }
}
