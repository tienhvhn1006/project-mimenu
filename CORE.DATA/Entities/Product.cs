﻿using CORE.DATA.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Product
    {
        [Key]
        public int Id { get; set; } = 0;

        public string Code { get; set; } = "";
        [StringLength(255)]

        [Display(Name = "Tiêu đề", Prompt = "Mời bạn nhập tiêu đề")]
        [Required]
        public string Name { get; set; } = "";

        [Display(Name = "Đường dẫn", Prompt = "Mới bạn nhập đường dẫn")]
        [Required]
        public string Sku { get; set; } = string.Empty;

        [Required]
        [Display(Name = "Chọn danh mục", Prompt = "Chọn danh mục")]
        public int ParentId { get; set; } = 0;
        [StringLength(255)]

        public string Image { get; set; } = "";
        [Display(Name = "Mô tả", Prompt = "Mời bạn nhập mô tả")]
        public string Description { get; set; } = "";
        [Display(Name = "Nội dung", Prompt = "Mới bạn nhập nội dung")]
        public string Content { get; set; } = "";

        public int? ViewCount { get; set; } = 0;
        [Display(Name = "Tiêu đề SEO", Prompt = "Mời bạn nhập tiêu đề SEO")]
        public string SeoPageTitle { set; get; } = "";

        [Display(Name = "Keywords SEO", Prompt = "Mời bạn nhập Keywords SEO")]

        [StringLength(255)]
        public string SeoKeywords { set; get; } = "";
        [Display(Name = "Description SEO", Prompt = "Mời bạn nhập Description SEO")]

        [StringLength(255)]
        public string SeoDescription { set; get; } = "";

        public DateTime DateCreated { set; get; } = DateTime.Now;
        public DateTime DateModified { set; get; } = DateTime.Now;
        public DateTime DatePublished { set; get; } = DateTime.Now;

        public string CreatedBy { get; set; } = "";
        public string ModifiedBy { get; set; } = "";
        [Display(Name = "Thứ tự", Prompt = "Thứ tự")]
        public int Order { get; set; } = 0;
        public string Properties { get; set; } = "";
        [Display(Name = "Trạng thái", Prompt = "Trạng thái")]
        public string Icon { get; set; } = "";
        public Status Status { set; get; } = Status.InActive;
        public TypeContent Type { set; get; } = TypeContent.News;
        public TypeNews TypePost { set; get; } = TypeNews.Blog;
        public string AppId { get; set; } = "";
        public bool IsDeleted { get; set; } = false;
        public void SetDefault()
        {
            this.Content = this.Content ?? "";
            this.Properties = this.Properties ?? "";
            this.AppId = this.AppId ?? "";
            this.Description = this.Description ?? "";
            this.Image = this.Image ?? "";
            this.Sku = this.Sku ?? "";
            this.SeoDescription = this.SeoDescription ?? "";
            this.SeoKeywords = this.SeoKeywords ?? "";
            this.SeoPageTitle = this.SeoPageTitle ?? "";

        }
    }
}
