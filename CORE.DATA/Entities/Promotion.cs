﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Promotion
    {
        public string Id { get; set; } = "";
        public string Code { get; set; } = ""; //Tạo ra code cho người dùng nhập
        public string Name { get; set; } = "";
        public double Discounts { get; set; } = 0;
        public Enums.TypePromotion Type { get; set; } = Enums.TypePromotion.Price;
        public double MaxValue { get; set; } = 0;
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;
        public DateTime DateCreated { set; get; } = DateTime.Now;
        public DateTime DateModified { set; get; } = DateTime.Now;
        public string CreatedBy { set; get; } = "";
        public string ModifiedBy { set; get; } = "";
        public bool IsVoucher { get; set; } = true; // True sẽ thuộc loại voucher cho thể cho người dùng nhập thêm 
        public Enums.Status Status { get; set; } = Enums.Status.Active;
        public string AppId { get; set; }
    }
}
