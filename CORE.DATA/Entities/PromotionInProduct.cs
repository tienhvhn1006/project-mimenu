﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CORE.DATA.Entities
{
    public class PromotionInProduct
    {
        [Key]
        public string Id { get; set; } = "";
        public int ProductId { get; set; } = 0;
        public int PromotionId { get; set; } = 0;

        public void AutoId()
        {
            this.Id = $"{this.ProductId}{this.PromotionId}";
        }
    }
}
