﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class Property
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Enums.Status Status { get; set; }
    }
}
