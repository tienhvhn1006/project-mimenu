﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class QRCode
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Image { get; set; } //Image QR Code
        public Enums.Status Status { get; set; }
        public string AppId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateModify { get; set; }
        public bool IsDeleted { get; set; }
        public QRCode()
        {
            this.Id = 0;
            this.Name = string.Empty;
            this.Url = string.Empty;
            this.Image = string.Empty;
            this.Status = Enums.Status.Active;
            this.AppId = string.Empty;
            this.CreatedBy = string.Empty;
            this.DateCreated = DateTime.Now;
            this.ModifiedBy = string.Empty;
            this.DateModify = DateTime.Now;
            this.IsDeleted = false;
        }

    }
}
