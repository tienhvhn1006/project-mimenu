﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CORE.DATA.Entities
{
    public class RolePermission
    {
        [Key, Column(Order = 0)]
        public string FunctionId { get; set; }
        [Key, Column(Order = 0)]
        public Enums.Action Action { get; set; }
        [Key, Column(Order = 0)]
        public Guid RoleId { get; set; }
    }
}
