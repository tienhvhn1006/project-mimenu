﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Entities
{
    public class TreeModel
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public int Sort { get; set; } = 0;
        public string Status { get; set; } = "";
        public List<TreeModel> Child { get; set; }
    }
    public class TreeModelMenu
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public int Sort { get; set; } = 0;
        public string Url { get; set; } = "";
        public string Icon { get; set; } = "";
        public List<TreeModelMenu> Child { get; set; }
    }

}
