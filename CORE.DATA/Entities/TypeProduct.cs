﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CORE.DATA.Entities
{
    public class TypeProduct
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int Id { get; set; } = 0;
        public int Quantity { get; set; } = 0;
        public int Unit { get; set; } = 1;
        public string AvatarArray { get; set; } = "";
        public double Price { get; set; } = 0;
        public double DiscountPercent { get; set; } = 0; 
        public double DiscountPrice { get; set; } = 0;
        public bool VAT { get; set; } = false;
        public int BrandId { get; set; } = 0;
        public string ColorId { get; set; } = "";
        public string BuyTogether { get; set; } = ""; //Sản phẩm mua kèm 
    }
}
