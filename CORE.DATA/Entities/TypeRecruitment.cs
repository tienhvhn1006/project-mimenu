﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CORE.DATA.Entities
{
    public class TypeRecruitment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int Id { get; set; } = 0;
        public DateTime Deadline { get; set; } = DateTime.Now;
        public string WorkingForm { get; set; } = "";
        public string Salary { get; set; } = "";
        public string Place { get; set; } = "";
        public string Business { get; set; } = "";
        public string Position { get; set; } = "";

    }
}

