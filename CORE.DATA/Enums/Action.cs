﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum Action
    {
        Read = 0,
        Write = 1,
        Create = 2,
        Delete = 3
    }
}
