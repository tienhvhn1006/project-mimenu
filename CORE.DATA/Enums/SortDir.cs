﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum SortDir
    {
        DESC = 1,
        ASC = 2,
    }
}
