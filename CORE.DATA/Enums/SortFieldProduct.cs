﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum SortFieldProduct
    {
        [EnumDescription("Sắp xếp")]
        All = 0,
        [EnumDescription("Ngày tạo")]
        DateCreated = 1,
        [EnumDescription("Ngày sửa")]
        DateModify = 2,
        [EnumDescription("Ngày xuất bản")]
        DatePublished = 3,
        [EnumDescription("Thứ tự")]
        Order = 4,
        [EnumDescription("Thứ tên")]
        Name = 5,
        [EnumDescription("Trạng thái")]
        Status = 6,
    }
}
