﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum Status
    {
        [EnumDescription("Ko Hoạt động")]
        InActive = 2,
        [EnumDescription("Hoạt động")]
        Active = 1,
        [EnumDescription("Tất cả")]
        ALL = 0,
    }
    public enum StatusOrder
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Đơn mới")]
        New = 1,
        [EnumDescription("Đã xác nhận")]
        Process = 2,
        [EnumDescription("Thành công")]
        Success = 3,
        [EnumDescription("Khách hủy")]
        Cancel = 4,
    }
    public enum StatusContact
    {
        [EnumDescription("Tất cả")]
        ALL = 0,
        [EnumDescription("Mới")]
        New = 1,
        [EnumDescription("Đã liên hệ")]
        Contact = 2,
        [EnumDescription("Đã xong")]
        Success = 3,
        [EnumDescription("Trục trặc")]
        Error = 4,

    }
}
