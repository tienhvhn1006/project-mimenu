﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum Transportation
    {
        [EnumDescription("Tất cả")]
        All =0,
        [EnumDescription("Ship code")]
        ShipCode = 1,
        [EnumDescription("Trực tiếp")]
        Pickup = 2,
    }
}
