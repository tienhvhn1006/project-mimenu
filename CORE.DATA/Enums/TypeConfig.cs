﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum TypeConfig
    {
        Input = 1,
        TextArea = 2,
        Image = 3,
        File = 4,
    }
}
