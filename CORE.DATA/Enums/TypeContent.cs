﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum TypeContent
    {
        All = 0,
        Zone = 1, //Danh mục 
        Product = 2,
        News = 3
    }
    public enum TypeNews
    {
        All = 0,
        Blog = 1,
        Recruitment = 2,
        Product = 3,
        Brand = 4,
    }
}
