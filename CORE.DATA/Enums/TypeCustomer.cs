﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum TypeCustomer
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Phổ biến")]
        Popular = 1,
        [EnumDescription("Tiềm năng")]
        Potential = 2,
        [EnumDescription("Vip")]
        VIP = 3,
        [EnumDescription("BlackList")]
        Backlist = 4,
    }
}
