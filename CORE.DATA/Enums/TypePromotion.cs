﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.DATA.Enums
{
    public enum TypePromotion
    {
        Price = 1,
        Percent = 2,
        FreeShip = 3,
    }
}
