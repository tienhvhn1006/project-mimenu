﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CORE.INFRASTRUCTURE.Interfaces
{
    public interface IRepository<T> where T : class
    {
        #region Query
        T FindById(int id);
        T FindById(string id);
        IQueryable<T> FindAll(string[] fileds = null);

        IQueryable<T> FindAll(Expression<Func<T, bool>> predicate, string[] fileds = null);

        bool Add(T entity, bool save = false);


        bool Update(T entity, bool save = false);

        bool Remove(T entity, bool save = false);

        bool Remove(int id, bool save = false);

        bool RemoveMultiple(List<T> entities, bool save = false);
        public bool IsExist(Expression<Func<T, bool>> predicate);
        #endregion



    }
}
