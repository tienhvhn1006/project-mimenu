﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.UTILITY
{
    public class GenericResult
    {
        public GenericResult()
        {
        }

        public GenericResult(bool success)
        {
            Success = success;
        }

        public GenericResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }
        public GenericResult(bool success, string message, string url)
        {
            Success = success;
            Message = message;
            Url = url;
        }
        public GenericResult(bool success, object data)
        {
            Success = success;
            Data = data;
        }
        public string Url { get; set; }
        public object Data { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public object Error { get; set; }
    }
}
