﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace CORE.UTILITY
{
    public class MUtility
    {
        private static readonly MD5 md5 = MD5.Create();
        public static string WebRootPath { get; set; }
        public static string LoadImage(string path, bool isThumb = true, string domain = "https://img.migroup.asia")
        {
            if (isThumb)
            {

                return domain + "/.tmb" + path;
            }
            else
            {
                return domain + path;
            }
        }
        public static string GetFileMd5Async(string fileName)
        {
            var bytes = Encoding.UTF8.GetBytes(fileName);
            return BitConverter.ToString(md5.ComputeHash(bytes)).Replace("-", string.Empty);
        }
        public const string UniChars =
       "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ";
        public const string UnsignChars =
            "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIIIOOOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU";


        public static string UnicodeToKoDau(string s)
        {
            string retVal = string.Empty;
            for (int i = 0; i < s.Length; i++)
            {
                int pos = UniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += UnsignChars[pos];
                else
                    retVal += s[i];
            }
            return retVal.ToLower();
        }


        const string strChar = "abcdefghijklmnopqrstxyzuvxw0123456789- ";
        public static string UnicodeToKoDauAndGach(string s)
        {

            s = UnicodeToKoDau(s.ToLower().Trim());


            //return sReturn.Replace("--", "-").ToLower();

            s = Regex.Replace(s, "[^a-zA-Z0-9]+", "-");
            s = Regex.Replace(s, "[-]+", "-");
            return s.Trim('-');
        }

        public static string UrlOrder(int id, string doamin)
        {
            return "https://" + doamin + $"/dat-hang-{id}";
        }
        public static string FomatMoney(object obj)
        {
            string data = string.Empty;

            if (!string.IsNullOrEmpty(obj.ToString()))
            {
                CultureInfo cultureInfo = new CultureInfo("vi-VN");
                data = String.Format(cultureInfo, "{0:C0}", obj);
            }
            else
            {
                data = "0 đ";
            }
            return data;
        }
        public static int BitWise(List<int> lstValue)
        {
            int res = 0;
            if (lstValue != null && lstValue.Count > 0)
            {
                foreach (var item in lstValue)
                {
                    if (item <= 30)
                    {
                        res = res | 1 << item;
                    }
                }
            }
            return res;
        }
        public static List<int> BitWise(int value)
        {
            List<int> res = new List<int>();
            if (value > 0)
            {
                int i = 0;
                double power = Math.Pow(2, i);
                while (power <= value)
                {
                    if ((1 << i & value) > 0)
                        res.Add(i);
                    i++;
                    power = Math.Pow(2, i);
                }
            }
            return res;
        }

        public static List<string> SplitStr(string str)
        {
            str = str ?? "";
            var data = str.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            return data.ToList();
        }
        public static string JoinStr(List<string> str)
        {
            var result = string.Join(",", str);

            return result;
        }
        public static string TimeEpochNow()
        {
            return ((long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
        }
        public static string GenCode(string First = "")
        {
            if (!String.IsNullOrEmpty(First))
            {
                return First + "_" + ((long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                return ((long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

        }

        public static Dictionary<string, int> AddMissingMonth(Dictionary<DateTime, int> DicResult, DateTime dateFrom)
        {
            var lastDate = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
            for (DateTime i = new DateTime(dateFrom.Year, dateFrom.Month, 1); i <= new DateTime(dateFrom.Year, dateFrom.Month, lastDate);)
            {
                try
                {
                    if (!DicResult.ContainsKey(i))
                    {
                        DicResult.Add(i, 0);
                    }
                }
                catch
                {

                }
                i = i.AddDays(1);
            }
            return DicResult.OrderBy(x => x.Key).ToDictionary(x => $"{x.Key.Day}/{x.Key.Month}", x => x.Value);
        }
        public static Dictionary<string, int> AddMissingYear(Dictionary<int, int> DicResult)
        {
            for (int i = 1; i <= 12; i++)
            {
                if (!DicResult.ContainsKey(i))
                {
                    DicResult.Add(i, 0);
                }
            }
            return DicResult.OrderBy(x => x.Key).ToDictionary(x => $"Tháng {x.Key}", x => x.Value);
        }

        public static Dictionary<string, int> AddMissingWeek(Dictionary<DateTime, int> DicResult, DateTime dateFrom)
        {
            for (DateTime i = dateFrom.AddDays(-6); i <= dateFrom;)
            {
                try
                {
                    if (!DicResult.ContainsKey(i))
                    {
                        DicResult.Add(i, 0);
                    }

                }
                catch
                {

                }
                i = i.AddDays(1);
            }
            return DicResult.OrderBy(x => x.Key).ToDictionary(x => $"{x.Key.Day}/{x.Key.Month}", x => x.Value);
        }
        public static Dictionary<string, double> AddMissingWeek(Dictionary<DateTime, double> DicResult, DateTime dateFrom)
        {
            for (DateTime i = dateFrom.AddDays(-6); i <= dateFrom;)
            {
                try
                {
                    if (!DicResult.ContainsKey(i))
                    {
                        DicResult.Add(i, 0);
                    }

                }
                catch
                {

                }
                i = i.AddDays(1);
            }
            return DicResult.OrderBy(x => x.Key).ToDictionary(x => $"{x.Key.Day}/{x.Key.Month}", x => x.Value);
        }
        public static Dictionary<string, double> AddMissingMonth(Dictionary<DateTime, double> DicResult, DateTime dateFrom)
        {
            var lastDate = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
            for (DateTime i = new DateTime(dateFrom.Year, dateFrom.Month, 1); i <= new DateTime(dateFrom.Year, dateFrom.Month, lastDate);)
            {
                try
                {
                    if (!DicResult.ContainsKey(i))
                    { DicResult.Add(i, 0); }
                }
                catch
                { }
                i = i.AddDays(1);
            }
            return DicResult.OrderBy(x => x.Key).ToDictionary(x => $"{x.Key.Day}/{x.Key.Month}", x => x.Value);
        }
        public static Dictionary<string, double> AddMissingYear(Dictionary<int, double> DicResult)
        {
            for (int i = 1; i <= 12; i++)
            {
                if (!DicResult.ContainsKey(i))
                {
                    DicResult.Add(i, 0);
                }
            }
            return DicResult.OrderBy(x => x.Key).ToDictionary(x => $"Tháng {x.Key}", x => x.Value);
        }

    }

    public static class PathHelper
    {
        public static string GetFullPathNormalized(string path)
        {
            return Path.TrimEndingDirectorySeparator(Path.GetFullPath(path));
        }

        public static string MapPath(string path, string basePath = null)
        {
            if (string.IsNullOrEmpty(basePath))
            {
                basePath = MUtility.WebRootPath;
            }
            path = path.Replace("~/", "").TrimStart('/').Replace('/', '\\');
            return GetFullPathNormalized(Path.Combine(basePath, path));
        }
    }
}
