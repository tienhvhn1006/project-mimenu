﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CORE.UTILITY.Object
{
    public class PromotionInfo
    {
        [JsonProperty("D")]
        public float Discounts { get; set; }
        [JsonProperty("T")]
        public int Type { get; set; }
        [JsonProperty("M")]
        public float MaxValue { get; set; }
    }
}
