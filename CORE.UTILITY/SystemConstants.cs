﻿using System;

namespace CORE.UTILITY
{
    public class SystemConstants
    {
        public class UserClaim
        {
            public const string Roles = "Roles";
            public const string Id = "Id";
            public const string Permissions = "Permissions";
            public const string FullName = "FullName";
            public const string Domain = "Domain";
            public const string AppId = "AppId";
            public const string Logo = "Logo";
            public const string SecurityScheme = "AppSecurityScheme";

        }
        public class Code_Adv
        {
            public const string Slide_Home = "Slide_Home";
            public const string Khoi_1 = "Khoi_1";
            public const string Khoi_2 = "Khoi_2";
            public const string Khoi_3 = "Khoi_3";
            public const string Khoi_LienHe = "Khoi_LienHe";
        }
    }
}
