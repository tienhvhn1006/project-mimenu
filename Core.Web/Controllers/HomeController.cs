﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using CORE.APPLICATION.Implementation;
using CORE.APPLICATION.Interfaces;

namespace Core.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public IAdvertisementsService _advertisementsService;
        public HomeController(ILogger<HomeController> logger, IAdvertisementsService advertisementsService)
        {
            _logger = logger;
            _advertisementsService = advertisementsService;
        }

        public IActionResult Index()
        {
            var AllQC = _advertisementsService.FindAll(x => x.Status == CORE.DATA.Enums.Status.Active && x.StartDate < DateTime.Now && x.EndDate > DateTime.Now, new string[] { "Code", "Info" }).ToList();

            return View(AllQC);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
